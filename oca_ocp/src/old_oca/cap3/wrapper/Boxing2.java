package old_oca.cap3.wrapper;

class Boxing2 {
	static Integer x;

	public static void main(String[] args) {
		// no value to unbox
		doStuff(x);
	}

	static void doStuff(int z) {
		int z2 = 5;
		System.out.println(z2 + z);
	}
}
