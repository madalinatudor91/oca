package old_oca.cap3.wrapper;

public class Parse {

	public static void main(String[] args) {
		
		// pentru fiecare cele 6 wrapper exista cate 6 xxxValue() -> 36 metode
		// int short long float double byte
		System.out.println(new Integer(3).longValue());
		System.out.println(new Integer(34534524).shortValue());
        System.out.println(new Short((short)4).longValue());

        // 6 metode, cate una pt fiecare
		int i = Integer.parseInt("9");
		
		// 6 metode, cate una pt fiecare + pt boolean
		Integer in = Integer.valueOf(9);
		
		Integer a1 = new Integer(1); // daca pun doar 1 => true, altfel false
		Integer b1 = Integer.valueOf(1); 
		if(a1 == b1){
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		
		System.out.println(Double.toString(3.24));
		// supraincarcare si cu baza
		System.out.println(Long.toString(4, 2));
		
		
	}
	
}
