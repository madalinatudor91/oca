package old_oca.cap3.wrapper;

public class EqualEqual {

	public static void main(String[] args) {

/*	
		In order to save memory, two instances of the
		following wrapper objects (created through boxing), will always be == when their
		primitive values are the same:
		Boolean
		Byte
		Character from \u0000 to \u007f (7f is 127 in decimal)
		Short and Integer from -128 to 127
*/		
		Integer i1 = 1000;
		Integer i2 = 1000;
		if(i1 == i2){
			System.out.println("1000 == 1000");
		}
		if(i1 != i2){
			System.out.println("1000 != 1000");
		}
		
		Integer i3 = 10;
		Integer i4 = 10;
		if(i3 == i4){
			System.out.println("10 == 10");
		}
		if(i3 != i4){
			System.out.println("10 != 10");
		}
		
	}
	
}
