package old_oca.cap3.wrapper;

import java.util.List;

public class Constructors {

	public static void main(String[] args) {
		// toate au inca un constructor cu String, exceptand Character
		Integer i1 = new Integer(5);
		Long l = new Long(3); // il citeste ca long intrucat:
		long l2 = 54;
		
		Boolean b = new Boolean("ff"); // orice e diferit de true e luat ca false
		Boolean b2 = new Boolean("tRUE");
		System.out.println(b + " " + b2); // btw, nu exista + intre booleans!
		
		// radix e baza
		Integer i2 = Integer.valueOf("000", 2);
		System.out.println(i2);
		
		Float.valueOf(2);
		Float.valueOf("4.4");
		
		
		int i = Integer.parseInt("4");
		Integer iWrapper = Integer.valueOf(4);
		int iValue = iWrapper.intValue();
		
		int[] array = {}; // acoladele astea sunt la baza Object
		
	}
	
}
