package old_oca.cap3.objects;

public class ModifyString {

	public static void main(String[] args) {
		String s1 = "PathRelativize";
		String s2 = s1;
		s1 = s1 + "b";
		// aici modificarile se fac doar asupra unui sir de caractere
		System.out.println(s1 + " - " + s2);
	}
	
}
