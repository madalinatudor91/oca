package old_oca.cap3.objects;

public class ModifyObject {

    static int property;

    public static void main(String[] args) {
        ModifyObject mo = new ModifyObject();
        mo.property = 3;
        ModifyObject mo2 = mo;
        mo2.property = -1;
        // !! se pastreaza aceeasi referinta, deci se modifica peste tot
        System.out.println(mo.property + " - " + mo2.property);

    }

}
