package old_oca.cap3;

import java.util.Arrays;

public class ValueParameter {

	public static void main(String[] args) {
		// se face o copie a lui 5 si se trimite in metoda
		bla(5);
		
		
//		When passing an object by reference, a reference to the object is copied into
//		the method. Changes to the object will be present in all references to that
//		object.
		
	}
	
	static void bla(int i){
		System.out.println(new int[2].getClass());
		System.out.println(new int[2][].getClass());
		
		//  arraycopy tine de System, nu de Arrays
        int[] source = {1, 2, 3, 4, 5};
        int[] dest = {-9, -8, -7, -6, -5};
		System.arraycopy(source, 2, dest, 3, 1);
        System.out.println(Arrays.toString(source));
        System.out.println(Arrays.toString(dest));
    }
	
}
