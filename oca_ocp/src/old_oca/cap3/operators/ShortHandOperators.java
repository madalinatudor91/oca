package old_oca.cap3.operators;

public class ShortHandOperators {

	public static void main(String[] args) {
		byte a = 10;
		System.out.println(a += 3);
		
		System.out.println(a -= 5);
		
		a = 10;
		System.out.println(a |= 2);

		a = 0;
		System.out.println(a |= 3); // 0 e un fel de false, restul e true?
		
		a = 50;
		System.out.println(a <<= 50);
		
		a = 50;
		// System.out.println(a <<<= 2);
		System.out.println(a >>>= 2);
		
		a = 50;
		System.out.println(a >>= 2);
		
	}
	
}
