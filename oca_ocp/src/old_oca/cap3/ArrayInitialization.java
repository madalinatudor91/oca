package old_oca.cap3;

public class ArrayInitialization {

	public static void main(String[] args) {
		int[] a = new int[3];
		System.out.println(a[2]);
		System.out.println(a); // hash code
		
		int c = 4;
		int x;
		if(c == 4){
			x = 7;
		}
		// System.out.println(x); // se prinde ca pe else nu e initializat
		
		String s = null;
		if(s != null){
			
		}
		
		ArrayClass array = new ArrayClass();
		System.out.println(array.a);
		
		Boolean b = true;
		//System.out.println(true || false + "");
		
		Integer bbbb = new Integer(3); // nu exista new Integer()
		int iii = 6;
		
		bbbb = iii; // autoboxing
		
		int v = bbbb.intValue(); // unboxing
		
		Integer aaa = iii;
		bbbb = 3;
		
		int j;
		//System.out.println(j);
		
		Flower2 f = new Flower2();
		System.out.println(f.j);
	}
	
}

class Flower2{
	
	int j;
}

class ArrayClass {

	int[] a;

}
