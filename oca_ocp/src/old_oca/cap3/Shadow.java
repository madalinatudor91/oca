package old_oca.cap3;

public class Shadow {

	static int size = 7;
	
	public static void main(String[] args) {
		System.out.println(size);
		changeIt(size);
		System.out.println(size);
	}

	private static void changeIt(int size) {
		size++;
		System.out.println(size);
	}
	
}
