package old_oca.cap3;

public class ReassignInMethod {

	public static void main(String[] args) {
		FlowerPower fp = new FlowerPower();
		fp.name = "gigi";
		doSomething(fp);
		System.out.println(fp.name);
	}

	private static void doSomething(FlowerPower fp) {
		fp.name = "gigica";
		
		// ce tare!
		// se permite intr-o metoda doar schimbarea starii unui obiect, nu si schimbarea referintei
		fp = new FlowerPower();
		fp.name = "gi";
		System.out.println(fp.name);
	}
	
}

class FlowerPower{
	
	String name;
	
}