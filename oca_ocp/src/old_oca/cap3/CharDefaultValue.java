package old_oca.cap3;

public class CharDefaultValue {

	char c;
	
	public static void main(String[] args) {

        // de ce zice asa?
		// A char has a default value of '\u0000', or 0,
		//when used as an instance variable.
		System.out.println("-" + new CharDefaultValue().c + "-");
	}
	
}
