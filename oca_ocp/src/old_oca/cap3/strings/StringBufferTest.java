package old_oca.cap3.strings;

public class StringBufferTest {

	// app, putem face clasa String, dar apoi nu mai gaseste corect metodele
	
	public static void main(String[] args) {
		
		// StringBuffer sb = ""; // nu poate converti din String in StringBuffer
		StringBuffer sb = new StringBuffer();
		sb.append(1); // compileaza fara cast-ul dat in exemplu carte
		
		StringBuffer sb2 = new StringBuffer('k'); // capacity
		StringBuffer sb3 = new StringBuffer(3); // aloca spatiu, ceva de genul
		
		String s = "ana";
		s.length();
		
		sb2.append(false);
		sb2.append("dsgfdfb");
		System.out.println(sb2);
		
		StringBuffer sb4 = new StringBuffer("sb");
		sb.append(true);
		System.out.println(sb4); // dubios
		
	}
	
}
