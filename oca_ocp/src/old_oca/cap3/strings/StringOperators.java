package old_oca.cap3.strings;

import java.util.Locale;

public class StringOperators {

	public static void main(String[] args) {
		String s = "Mada";
		System.out.println(s.charAt(1));
		
		System.out.println(s.indexOf('a'));// primeste ca parametru int, dar stie sa-l transforme
		
		// aici nu da String index out of bound exception, la charAt ar da
		System.out.println(s.indexOf('8', 100));
		
		s = s.replace('A', 'n');
		// e case sensitive :)
		System.out.println(s);
		
//		System.out.println(s.substring(5, 6)); // StringOut...Exc
//		System.out.println(s.substring(1, 0)); // StringOut...Exc
		
		String nimic = s.substring(1, 1);
		System.out.println("+" + nimic + "+"); // nimic = ""
		
		// fiecare pe treaba lui...
		s.toLowerCase(Locale.TRADITIONAL_CHINESE);
		
	}
	
}
