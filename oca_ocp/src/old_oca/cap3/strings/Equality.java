package old_oca.cap3.strings;

public class Equality {

	public static void main(String[] args) {
		String s1 = "";
		String s2 = new String("");
		String s3 = new String();
		
		// acelasi lucru la s1
		if(s3 == s2){
			System.out.println("Egale");
		} else {
			System.out.println("Inegale");
		}
		
		if(s3.equals(s2)){
			System.out.println("Egale");
		} else {
			System.out.println("Inegale");
		}
		
	}
	
}
