package old_oca.cap3.strings;

public class StringTest {

	private static void test() {
		String s = "s";
		s += "g";
		System.out.println(s);
		
		byte b = 3;
		System.out.println(b += 5);
		
		// nu tb toString sau concatenat cu altceva
		System.out.println(4);
		
		int a = 25;
		// desigur, se aproximeaza
		a /= 3;
		System.out.println(a);
		
		char c = '2';
		//c = '020';
		// nu permite c = '20';
		
		// syntax error, delete this token
		//if(true === false){
			
		//}
		
	}
	
	public static void main(String[] args) {
		test();
		
		
		String[] ss = new String[4];
		int l = ss.length;
		System.out.println(l);
	}
	
}
