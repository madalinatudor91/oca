package old_oca.cap3;

public class Equals {

	public static void main(String[] args) {
		Flower f = new Flower();
		f.name = "f";
		Flower f2 = new Flower();
		f2.name = "f";
		System.out.println(f.equals(f2)); // inainte de a implementa equals si hash code dadea false
	}
	
}

class Flower{
	
	String name;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Flower other = (Flower) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
}