package old_oca.cap3.overload;

class AddBoxing {
	static void go(Integer x) {
		System.out.println("Integer");
	}

	static void go(long x) {
		System.out.println("long");
	}

	public static void main(String[] args) {
		int i = 5;
		go(i); // which go() will be invoked?
		// widening over unboxing
	}
}

class AddVarargs {
	static void go(int x, int y) {
		System.out.println("int,int");
	}

	static void go(byte... x) {
		System.out.println("byte... ");
	}

	public static void main(String[] args) {
		byte b = 5;
		go(b, b); // which go() will be invoked?
		// widening over var-args
	}
}

class BoxOrVararg {
	static void go(Byte x, Byte y) {
		System.out.println("Byte, Byte");
	}

	static void go(byte... x) {
		System.out.println("byte... ");
	}

	public static void main(String[] args) {
		byte b = 5;
		go(b, b); // which go() will be invoked?
		// unboxing over var-args
	}
}