package old_oca.cap3.gc;

import java.util.Date;

class GarbageTruck {
	public static void main(String[] args) {
		StringBuffer s1 = new StringBuffer("hello");
		StringBuffer s2 = new StringBuffer("goodbye");
		System.out.println(s1);
		// At this point the StringBuffer "hello" is not eligible
		s1 = s2; // Redirects s1 to refer to the "goodbye" object
		// Now the StringBuffer "hello" is eligible for collection
	}

	
	/*
In the preceding example, we created a method called getDate() that returns a
Date object. This method creates two objects: a Date and a StringBuffer containing
the date information. Since the method returns the Date object, it will not be
eligible for collection even after the method has completed. The StringBuffer object,
though, will be eligible, even though we didn't explicitly set the now variable to null.
	 */
	public static Date getDate() {
		Date d2 = new Date();
		StringBuffer now = new StringBuffer(d2.toString());
		System.out.println(now);
		
		// singleton
		Runtime.getRuntime().gc(); // <=>
		System.gc();
		
		return d2;
	}

}
