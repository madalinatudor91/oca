package old_oca.cap6;

public class For {

	public static void main(String[] args) {
		String[] s = { "a", "b" };

		// pentru ca foloseste o copie, nu se redefineste, e ca in metode
		for (String string : s) {
			string = new String("j");
		}
		for (String string : s) {
			System.out.println(string);
		}

	}

}

