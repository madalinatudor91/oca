package old_oca.cap6;

class InitError {
	static int[] x = new int[4];
	static {
		x[4] = 5;
	} // bad array index!

	//Finally, if you make a mistake in your static init block, the JVM can throw an
	//ExceptionInInitializationError.
	public static void main(String[] args) {
	}
}
