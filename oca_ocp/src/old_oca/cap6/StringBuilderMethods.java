package old_oca.cap6;

public class StringBuilderMethods {

	public static void main(String[] args) {
		StringBuilder s = new StringBuilder();
		//s.append("abcde", 2, 6);// Index Out of bounds
		s.append("abcde", 2, 4);
		System.out.println(s);
		
		// 
		s.appendCodePoint(66); // append al charului cu nr specificat
		System.out.println(s);
		
		// metoda de delete in string?
		s.delete(0, 0);
		System.out.println(s);

		s.insert(2, "123456789", 3, 4);
		System.out.println(s);
		
		StringBuilder ss = new StringBuilder("0123456");
		System.out.println("\n" + ss.delete(2, 5));
		
		boolean b = true;
		System.out.println(b |= false);
        System.out.println(b != false);
	}
	
}
