package old_oca.cap6;

public class PlusPlus {

	public static void main(String[] args) {
		int i = 1;

		// 1 + ++i;
		// 1++ -> i =2
		// 1 + ++2 = 1 + 3
		System.out.println(i++ + ++i);
        System.out.println(i);

        int j = 1;
		// 1 + ++j + ++j // j=2
		// 1 + 3 + 4
		System.out.println(j++ + ++j + ++j);
	}
	
}
