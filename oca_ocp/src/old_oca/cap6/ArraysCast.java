package old_oca.cap6;

public class ArraysCast {

	public static void main(String[] args) {
		// int[] a = new float[4]; // nu merge nici cu cast pt ca sunt primitive
		
		// nu stie sa converteasca referinta de int catre float
		// float[] f = new int[5];
		int[] a = { 3, (int) 4.3, 3 };
		// Integer[] i = new Float[4];
		// Float[] f = new Integer[4];

		Object[] o = new Test[4]; // merge array din parent in child
		for (int i = 0; i < o.length; i++) {
			o[i] = new Test();
			((Test) o[i]).nimic();
		}
		// Test[] t = new Object[4];
	}

}

class Test {
	
	void nimic(){
		
	}
}