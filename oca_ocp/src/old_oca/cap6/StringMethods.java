package old_oca.cap6;

import java.io.Serializable;

public class StringMethods implements Serializable {

	public static void main(String[] args) {
		
		String s = "abcde";
		System.out.println(s.charAt(1)); // String Index Out of Bounds daca nu e
		
		System.out.println("\n" + s.endsWith("de"));
		//s.endsWith('c');
		System.out.println(s.startsWith("b"));
		System.out.println(s.startsWith("b", 1)); // asta are doi parametrii, endsWith nu prea are sens sa aiba
		
		
		System.out.println("\n" + s.indexOf('b')); // primeste int, dar de fapt e pt char ca utilitate...
		System.out.println(s.indexOf("bc")); // daca nu gaseste returneaza -1
		System.out.println(s.indexOf('d', 2)); // cauta incepand cu pozitia 2; similar metoda pt string
		
		// metoda cu CharSequence
		String s2 = s.replace(new StringBuilder("a"), new StringBuilder("1"));
		System.out.println("\n" + s2);
		// exista supraincarcare cu char
		
		//CharSequence interface
		// length()
		// charAt()
		// subSequence(int arg0, int arg1): CharSequence
		
		
//		s.substring(0, 6); // String index out of bounds!
		
		System.out.println("\n" + s.subSequence(0, 6)); // String index out of bounds!
		
	}
	
}
