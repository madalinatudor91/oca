package old_oca.cap5;

import java.util.ArrayList;
import java.util.List;

public class ForEnhanced {

	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList();
		list.add(2);
		list.add(3);
		Integer i;
//		for (i : list) {
//
//		}
		
		for(Test t : Test.values()){
			t = Test.t2;
		}
		
		for(final Test t : Test.values()){
			
		}

		long[] longArray = {1, 3, 5};
		//for ((int)long l : longArray) {
			
		//}
		
		System.out.println(Character.MIN_VALUE + " char " + Character.MAX_VALUE);
		System.out.println(Byte.MIN_VALUE + " byte " + Byte.MAX_VALUE);
		System.out.println(Short.MIN_VALUE + " short " + Short.MAX_VALUE);
		System.out.println(Integer.MIN_VALUE + " int " + Integer.MAX_VALUE);
		System.out.println(Long.MIN_VALUE + " long " + Long.MAX_VALUE);
		System.out.println(Float.MAX_EXPONENT + " " + Float.MAX_VALUE);
		System.out.println(Double.MAX_EXPONENT + " " + Double.MAX_VALUE);
	}
	
	enum Test{ // public or default
		t1, t2, t3
	}
	
}
