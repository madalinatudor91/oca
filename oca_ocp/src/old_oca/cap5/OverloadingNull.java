package old_oca.cap5;

class OverloadingNull {

    // ia aminte la overloading si apel cu null
	public static void main(String[] args) {
        Object o = null;
//        gigi(o); // cannot resolve method

		// ambiguous
	//	gigi(null);
	}
	
	static void gigi(String i){
		System.out.println("string");
	}
	
	static void gigi(Integer i){
		System.out.println("integer");
	}
	
}
class AnotherTest {

    public static void main(String[] args) {
        gigi(null);
    }

    static void gigi(CharSequence cs) {
        System.out.println("cs");
    }

    static void gigi (String s) {
        System.out.println("s");
    }

}
