package old_oca.cap5;

public class Exceptions {

	/*
The getMessage method returns a detailed message about the exception.
The toString method returns detailed messages about the exception and a class name.
The printStackTrace method prints a detailed message, the class name, and a
stack trace.
	 */

	public static void main(String[] args) {
		try {
			throw new Exception();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
}
