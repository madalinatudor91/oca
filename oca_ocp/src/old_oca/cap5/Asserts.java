package old_oca.cap5;

public class Asserts extends Object {


	/*
Don't Use Assertions to Validate Arguments to a Public Method
A public method might be called from code that you don't control (or from code
you have never seen).

Do Use Assertions to Validate Arguments to a Private Method

Don't Use Assertions to Validate Command-Line Arguments

Do Use Assertions, Even in Public Methods, to Check for Cases
that You Know Are Never, Ever Supposed to Happen

The rule is, an assert expression should leave the program in the same state it was
in before the expression!
	
	 */
	
	private void doStuff() {
		//java -ea com.geeksanonymous.TestClass
		// de la java 4 assert e cuv cheie
		// or
		// java -enableassertions com.geeksanonymous.TestClass

		/*
			You must also know the command-line switches for disabling assertions,
			java -da com.geeksanonymous.TestClass
			or
			java -disableassertions com.geeksanonymous.TestClass
		 */
		
		/*
			java -ea:com.foo... Enable assertions in package com.foo and any of its subpackages.
		 */
		
		int x = 3; int y = 4;
		assert (y > x);
		assert(y > x): "y " + y + " x " + x; // ideea e ca expresia dupa : tre; sa returneze ceva
		// more code assuming y is greater than x
	}

}
