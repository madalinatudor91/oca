package old_oca.cap5;

public abstract class OverloadingFloatInt {

	static public void main(String[] args) {
		
	}

	abstract public void test();
	
	static void test2(){} // ma obliga sa pun ori static ori abstract, nu amandoua
	
}


/*private*/ class Suprascrie extends OverloadingFloatInt{

	@Override
	public void test() {

	}

//    @Override
//    public void test2() {}
}
