package old_oca.cap5;
class SubException extends Exception {
}

class SubSubException extends SubException {
}

public class ExceptionInheritance {
	void doStuff() throws SubException {
	}
}

// deci se poate cu ceva mai specific
class CC2 extends ExceptionInheritance {
	void doStuff() throws SubSubException {
	}
}

class CC3 extends ExceptionInheritance {
//	void doStuff() throws Exception {
//	}
}

class CC4 extends ExceptionInheritance {
	void doStuff(int x) throws Exception {
	}
}

class CC5 extends ExceptionInheritance {
	void doStuff() {
	}
}