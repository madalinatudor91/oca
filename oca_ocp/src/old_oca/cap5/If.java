package old_oca.cap5;

public class If {

	public static void main(String[] args) {
		
		if(true){
			
		} else {
			
		}
		
		/*
		But the only variables that can be assigned (rather
				than tested against something else) are a boolean or a Boolean; all other assignments
				will result in something non-boolean, so they�re not legal, as in the following:
		*/
		int x = 3;
//		if (x = 5) { }
	}
	
}
