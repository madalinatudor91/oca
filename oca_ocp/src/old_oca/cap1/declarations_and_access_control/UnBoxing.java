package old_oca.cap1.declarations_and_access_control;

public class UnBoxing {

	public static void main(String[] args) {
		Short s = 8;
		la(s);
	}

	// correct call unboxing, incorrect with Long
	static void la(int x){
		System.out.println(x);
	}
	
}
