package old_oca.cap1.declarations_and_access_control;

class Animal {
}

class Horse extends Animal {
	void ceva() {
		System.out.println("ceva");
	}
}

class Pig extends Animal {

}

class UseAnimals {
	static void doStuff(Animal a) {
		System.out.println("In the Animal version");
	}

	static void doStuff(Horse h) {
		System.out.println("In the Horse version");
	}

	public static void main(String[] args) {
		Animal animalObj = new Animal();
		Horse horseObj = new Horse();
		Animal a = new Horse();
		doStuff(animalObj);
		doStuff(horseObj);

		// merge dupa regulile overloadingului pt ca nu e a.doStuff()
		doStuff(a);

		/*
		 * The new and improved code block contains a cast, which in this case is sometimes called a downcast, because
		 * we're casting down the inheritance tree to a more specific class.
		 */
		if (a instanceof Horse) {
			Horse d = (Horse) a; // casting the ref. var.
			d.ceva();
		}

		/*
		 * upcasting (casting up the inheritance tree to a more general type)
		 */

	}
}

class UseZoo {

	static void doStuff(Pig a) {
		System.out.println("In the Pig version");
	}

	static void doStuff(Horse h) {
		System.out.println("In the Horse version");
	}

	public static void main(String[] args) {
		Animal a = new Animal();
//		doStuff(a);
	}
}
