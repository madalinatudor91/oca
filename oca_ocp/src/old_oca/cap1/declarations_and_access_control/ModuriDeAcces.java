package old_oca.cap1.declarations_and_access_control;

// import cap1.DeclarationsAndAccessControl.defaultAccess.DefaultClass;
import java.lang.Integer;

// only public, final and abstract are allowed
public class ModuriDeAcces {

	Integer myInteger;
	
	public void moduriAccesInMetoda() {
		// zice ca doar final e permis
		// private int t;
	}
	
	// nu e vizibil
//	 DefaultClass dc;

}
