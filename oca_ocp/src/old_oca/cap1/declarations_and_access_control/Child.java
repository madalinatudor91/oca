package old_oca.cap1.declarations_and_access_control;

import old_oca.cap1.declarations_and_access_control.protected_pack.Parent;

public class Child extends Parent {

	public static void main(String[] args) {
		new Child().parentMember = 5;
	}
	
}
class UseChild {

	public static void main(String[] args) {
		Child c = new Child();

		// doar in Child e acces la x, prin super
		// the field Parent.x is not visible
		// devine private
		//c.x = 5;
		
	}
	
}
