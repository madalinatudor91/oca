package old_oca.cap1;

public class ByteOperators {
/*
	~       Unary bitwise complement
	<<      Signed left shift
	>>      Signed right shift
	>>>     Unsigned right shift
	&       Bitwise AND
	^       Bitwise exclusive OR true ^ true = > false
	|       Bitwise inclusive OR / XOR
*/


    public static void main(String[] args) {
        System.out.println(true ^ true);
        System.out.println(true ^ false);
        System.out.println(false ^ false);

        System.out.println(true & false);

        System.out.println(true | true);
        System.out.println(true | false);
        System.out.println(false | false);

        System.out.println(!false);
    }
}
