package old_oca.cap1.understand_class_structure;

public class InitializeArray {

	// e oarecum static
	public enum MyEnum{
		ENUM1, ENUM2
	}
	
	
	public static void main(String[] args){
		// !!
		String[] vect1 = {};
		// String[] vect2 = [];
		
		int[] vect3 = new int[20];
		int[] vect4 = {3, 4, 5};
		int[] vect6 = {};
		//int[] vect5 = [3, 5];
		
		Integer[] ii = new Integer[4];
		System.out.println(ii[3] + " - " + vect3[3]);
		
	}
	
}
