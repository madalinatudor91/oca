package old_oca.cap1.comparable_comparator;


import java.util.Arrays;
import java.util.Comparator;


public class Fruit implements Comparable<Fruit>{

	private int valability;
	
	private int quantity;

	public Fruit(int quantity, int valability) {
		this.quantity = quantity;
		this.valability = valability;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getValability() {
		return valability;
	}

	public void setValability(int valability) {
		this.valability = valability;
	}

	@Override
	public String toString() {
		return this.getQuantity() + " " + this.getValability();
	}
	
	@Override
	public int compareTo(Fruit o) {
		return this.quantity - o.getQuantity();
	}
	
	public static Comparator<Fruit> comparator = new Comparator<Fruit>() {

		@Override
		public int compare(Fruit o1, Fruit o2) {
			return o1.getValability() - o2.getValability();
		}
	};
	
	public static void main(String[] args) {
		Fruit[] fruits = {
				new Fruit(5, 2), new Fruit(9, 1), new Fruit(2, 5)
		};
		System.out.println("Sortare default dupa quantity");
		Arrays.sort(fruits);
		for (int i = 0; i < fruits.length; i++) {
			System.out.println(fruits[i].toString());
		}
		
		System.out.println("Sortare dupa valabilitate");
		Arrays.sort(fruits, comparator);
		for (int i = 0; i < fruits.length; i++) {
			System.out.println(fruits[i].toString());
		}
	}

}
