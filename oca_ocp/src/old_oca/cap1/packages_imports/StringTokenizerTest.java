package old_oca.cap1.packages_imports;

import java.util.StringTokenizer;

public class StringTokenizerTest {

	public static void main(String[] args) {
		String gogu = "Gogu are\nmere";
		// daca nu ii dam noi parametru delimitatorul, el cd si \n, \t si " " si altele
		StringTokenizer st = new StringTokenizer(gogu);
		System.out.println(st.countTokens());
	}
	
}
