package old_oca.cap1.packages_imports;

import java.io.Console;

public class ConsoleExercise {

	public static void main(String[] args) {
		// strict in cmd, altfel null pointer
		Console console = System.console();
		System.out.println("Introduceti un sir de caractere: ");
		String sirIntrodus = console.readLine();
		System.out.println(sirIntrodus);
		
	}
	
}
