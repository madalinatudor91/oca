package old_oca.cap1.packages_imports;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class FileLogger {

	/*
			SecurityException extends RuntimeException: Usually thrown by
			the JVM. It is thrown by the security manager upon security violation. For
			example, when a java program runs in a sandbox (such as an applet) and it
			tries to use prohibited APIs such as File I/O, the security manager throws
			this exception.
	 */
	
	public static void main(String[] args) throws SecurityException, IOException {
		
		new File("logger").mkdir();
		String logDate = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
		StringBuilder logFileName = new StringBuilder();
		logFileName = logFileName.append("logger//log-").append(logDate).append(".txt");
		
		// !!! atentie la compilation fails: throws Exception pe unde e cazul
		FileHandler fileHandler = new FileHandler(logFileName.toString());
		fileHandler.setFormatter(new SimpleFormatter());
		
		Logger logger = Logger.getLogger("my logger");
		logger.setLevel(Level.ALL);
		logger.addHandler(fileHandler);
		
		logger.info("Te informez ca...");
		
		fileHandler.close();
	}
	
}
