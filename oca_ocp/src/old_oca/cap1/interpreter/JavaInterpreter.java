package old_oca.cap1.interpreter;

import java.util.Properties;

public class JavaInterpreter {

	private java.util.List<Byte> list;
	
	public static void main(String[] args) {
		Properties properties = System.getProperties();
		properties.setProperty("key", "value");
		System.out.println(properties);
		System.out.println("*********");
		properties.list(System.out);
		
		System.out.println("*********");
		System.out.println(properties.getProperty("gigi", "nu exista"));
	}

	public static class ClassToBeInterpreted {

        // pg 28 - 29
        /*
         * rulat:
        java -Dkey2=value2 ClassToBeInterpreted.java -list_all
                Error: Could not find or load main class ClassToBeInterpreted.java
         */

        public static void main(String[] args) {
            if (args.length == 0) {
                System.exit(0);
            } else {
                Properties properties = System.getProperties();
                properties.setProperty("key1", "value1");
                switch (args[0]) {
                case "-list_all":
                    properties.list(System.out);
                    break;
                case "-list_prop":
                    properties.get(args[1]);
                    break;
                default:
                    System.out
                            .println("Utilizare:\njava ClassToBeInterpreted [-list_all]\njava ClassToBeInterpreted [-list_prop [property]]");
                    break;
                }

            }
        }

    }
}
