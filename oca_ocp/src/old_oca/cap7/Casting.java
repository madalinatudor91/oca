package old_oca.cap7;

public class Casting {

	public static void main(String[] args) {
		
		A a = new A();
		B b = new B();
		b.gigi = 9;
		System.out.println(b.gigi);
		
		a = b;
        System.out.println(((B)a).gigi);
        //a.gigi;
		b = (B) a; // pastreaza valoarea
        System.out.println(b.gigi);
	}
	
}

class A {
	
}

class B extends A{
	int gigi;
}