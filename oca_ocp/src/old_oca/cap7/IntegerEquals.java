package old_oca.cap7;

public class IntegerEquals {

	public static void main(String[] args) {
		int i = 40000; // equals intre int si interger, cu valoare mare, da true
		/* takes an additional
		argument, int radix, which indicates in what base (for example binary, octal, or
		hexadecimal) the first argument is represented
		*/

		Integer iW = new Integer(40000);
		if(iW.equals(i)){
			// tb sa satisfaca si conditia de instanceof si se intampla asta pentru ca se face autoboxing pentru a putea aplica equals
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		
		//Short sW = new Short(3);
		Short sw = new Short( (short) 4);
		if(sw.equals(iW)){
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		
	}
	
}
