package old_oca.cap7;

import java.util.ArrayList;
import java.util.List;

public class ArrayListConstructors {

	public static void main(String[] args) {
		
		// The size of an ArrayList is based on the number of objects in it, not its capacity.
		List l = new ArrayList<String>(3);
		l.add(2, "s"); // index out of bound
		System.out.println(l.get(0));
		
		new ArrayList<>(l); // Collection

//		ArrayList objects have more overhead involved than standard arrays. However,
//		their flexibly often more than makes up for this.
	}
	
	
}
