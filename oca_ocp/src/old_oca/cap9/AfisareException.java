package old_oca.cap9;

public class AfisareException {

	public static void main(String[] args) {
		try {
//			Integer i = null;
//			if(i == 3){
//				System.out.println("Catch me!");
//			}
            throw new IllegalArgumentException("mesaj eroare");
		} catch (Exception e) {
			// When you use System.out.println(exception), a stack trace is not printed. Just
			// the name of the exception class and the message is printed.
			System.out.println("Just exception");
			System.out.println(e);
			
			//The getMessage method returns a detailed message about the exception.
			System.out.println("\nGet Message:");
			System.out.println(e.getMessage());

			//The toString method returns detailed messages about the exception and a class name.
			System.out.println("\nTo String");
			System.out.println(e.toString());
			
			System.out.println("\nGet Stack Trace");
			System.out.println(e.getStackTrace());
			
			//The printStackTrace method prints a detailed message, the class name, and a stack trace.
			System.out.println("\nPrint Stack Trace");
			e.printStackTrace();

            System.out.println("\nLocalized message");
            System.out.println(e.getLocalizedMessage());
		}
	}
	
}
