package old_oca.mixt;

public class Main {

	public static void main(String[] args) {
		// apelat fara argumente, nu va fi niciodata null
		System.out.println(args);
		//System.out.println(args[3]); // ArrayIndexOutOfBounds
		
		int[] i = null;
		System.out.println(i[4]); // Null pointer
		
	}
	
	// String, StringBuilde, StringBuffr and System are final classes
	// Integer, Boolean, Character etc are also final calsses
	
	// clasa abstracta din java.lang, extinsa de Short, Integer, Long, Float, Double
	// nu e extinsa de Character
	Number n = new Number() {
		
		@Override
		public long longValue() {
			return 0;
		}
		
		@Override
		public int intValue() {
			return 0;
		}
		
		@Override
		public float floatValue() {
			return 0;
		}
		
		@Override
		public double doubleValue() {
			return 0;
		}
	};
	
}
