package old_oca.mixt;

public class ShortByteInt {

	public static void main(String[] args) {
		byte b = 1;
		char c = 1;
		short s = 1;
		int i = 1;
		
		// cannot convert from int to short
		//s = b * b;
		
		i = b << b;
		
		s <<= b;
		
		// cannot convert from int to char
		//c = c + b;
		
		s += i;
		
		
		short x = 4;
        // works
		x += 3.4; // <=> x = (short) x + 3.4;
//        x =x + 6.2;
		
	}
	
}
