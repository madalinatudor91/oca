package old_oca.mixt;

public class IntegerImmutable {

	public static void main(String[] args) {
		Integer i = 5;
		doSomething(i);
		System.out.println(i);
	}
	
	static void doSomething(Integer i){
		i++;
	}
}
