package old_oca.mixt;

public class Gc {

	public Object getObject(Object a) // 0
	{
		/*
			After which line will the object created at line XXX be eligible for garbage
			collection?
		*/
		
		Object b = new Object(); // XXX
		Object c, d = new Object(); // 1
		c = b; // 2
		b = a = null; // 3
		return c; // 4
	}

	/*
			Never in this method.
			Note that at line 2, c is assigned the reference of b. i.e. c starts pointing to the object
			created at //XXX. So even if at //3 b and a are set to null, the object is not without any
			reference.
			Also, at //4 c is being returned. So the object referred to by c cannot be garbage
			collected in this method!
	 */
	
	
	
	// When is the Object created at line //1 eligible for garbage collection?
	
	public Object getObject() {
		Object obj = new String("aaaaa"); // 1
		Object objArr[] = new Object[1]; // 2
		objArr[0] = obj; // 3
		obj = null; // 4
		objArr[0] = null;// 5
		return obj; // 6
	}
	
	
	// D. Just after line 5.
	// pentru ca nu mai are referinta catre obj
	
	
}
