package old_oca.mixt;

public class Chars {

	public static void main(String[] args) {
		String a = "Supercalifragilisticexpialidocious!";
		String b = a.substring(9, 15); // fragil
		char[] c = { a.charAt(3), a.charAt(34) };
		System.out.println(b + String.valueOf(c)); // cum adica? valueOf
		
		String s = "d   r";
		// no args on trim method
		System.out.println(s.trim());
		
		String t = "gigi are mere";
		// CharSequence ca param
		System.out.println(t.replace("mere", "pere"));
		
		StringBuilder db = new StringBuilder();
		db.append(2);
		db.append((short)2); // merge, dar nu exista metoda ce primeste short
		
		StringBuilder sb = new StringBuilder("magic");
		sb.append(false);
		System.out.println(sb);
		sb.replace(1, 3, "us");
//		sb.matches("musical");
		
		a = "music";
		// primeste regex
		System.out.println(a.matches("music"));
		
	}

}
