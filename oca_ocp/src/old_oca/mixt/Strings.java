package old_oca.mixt;

public class Strings {

	public static void main(String[] args) {
		String s = "s";
		System.out.println("0 " + s.substring(0));
		System.out.println("1 " + s.substring(1));// interesant
		//System.out.println("4 " + s.substring(4)); // StringIndexOutOfBounds
		
		/**
		public String substring(int beginIndex, int endIndex)- Returns a
		new string that is a substring of this string. The substring begins at the specified
		beginIndex and extends to the character at index endIndex - 1. 
		
		Thus the length of the substring is endIndex-beginIndex.
		 */
		
		System.out.println("\n\n");
		String ss = "abcde";
		System.out.println(ss.substring(1, 2));
		//System.out.println(ss.substring(3, 1)); // StringIndexOutOfBounds
		//System.out.println(ss.substring(-5, 6));
		System.out.println(ss.substring(2, 4));
		
		System.out.println("\n\n");
		String sss = "abcde";
		//sss.delete nu exista!
		
		//StringBuilder sb = "abcde";
		//String str = new StringBuilder();
		//StringBuilder str = new String();
		
		// tot end - init nr caractere sterge
		StringBuilder sb = new StringBuilder("abcde");
		sb.delete(1, 4);
		System.out.println(sb);
		//sb.deleteCharAt(5);
		sb.deleteCharAt(1);
		System.out.println(sb);
		
		System.out.println("\n\n");
		StringBuilder sbb = new StringBuilder("abcde");
		sbb.delete(0, 0); // nothing happens
		System.out.println(sbb);
		sbb.delete(4, 100);// se comporta frumos, chiar daca nu are atatea caractere
		System.out.println(sbb);


		sbb.ensureCapacity(23);
        System.out.println(sbb.length());
    }
	
}
