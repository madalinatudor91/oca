package old_oca.mixt.iterator2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Facultate implements Iterable<Integer>{

	private List<Integer> sali;
	
	private int pozitieCurenta;
	
	public Facultate() {
		sali = new ArrayList<>();
		sali.add(1);
		sali.add(4);
		sali.add(3);
	}

	@Override
	public Iterator<Integer> iterator() {
		Iterator<Integer> iterator = new Iterator<Integer>() {

			@Override
			public boolean hasNext() {
				return pozitieCurenta < sali.size();
			}

			@Override
			public Integer next() {
				if (pozitieCurenta < sali.size()) {
					Integer sala = sali.get(pozitieCurenta);
					pozitieCurenta++;
					return sala;
				} else {
					return null;
				}
			}

			@Override
			public void remove() {
				if (pozitieCurenta < sali.size()) {
					sali.remove(pozitieCurenta);
				}
			}
		};
		return iterator;
	}

	
	
}
