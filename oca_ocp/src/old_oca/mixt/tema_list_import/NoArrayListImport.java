package old_oca.mixt.tema_list_import;

import java.lang.reflect.Method;


public class NoArrayListImport {

	public static void main(String[] args) throws Exception {

		// cum specific new ArrayList<String>?
		// Generics are a compile-time only "trick
		// Reflection is runtime-only.

		// ClassLoader classLoader = NoArrayListImport.class.getClassLoader();
		// Class<?> c = classLoader.loadClass("java.util.ArrayList");

		// sau

		Class<?> c = Class.forName("java.util.ArrayList");
		Object constructor = c.getConstructor().newInstance();
		Method addMethod = constructor.getClass().getMethod("add", Object.class);
		Object o1 = new String("gigi");
		
		addMethod.invoke(o1);
	}

}
