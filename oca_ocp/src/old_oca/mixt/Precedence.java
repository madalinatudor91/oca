package old_oca.mixt;

public class Precedence {

	public static void main(String[] args) {
		int a, b, c;
		a = b = c = 0;
		System.out.println(a++ + " " + --b + " " + c++ + " " + a + " " + b + " " + c);

		int x = 2;
		int y = 3;
		// The first expression compares x and y, and the result is false, because the
		// increment on x doesn't happen until after the == test is made
		if ((y == x++) | (x == 3) || (x < ++y)) {
			System.out.println("1. x = " + x + " y = " + y);
		}

		// pana la urma | precede ||?
		x = 2;
		y = 3;
		if ((y == x++) || (x == 3) | (x < ++y)) {
			System.out.println("x = " + x + " y = " + y);
		}

        int m = 9, n = 8;
        System.out.println(m +++ n);
        System.out.println(m);
        System.out.println(n);

        int k = 3;
        System.out.println(k++ * 5); // 3++ => variable expected
    }

}
