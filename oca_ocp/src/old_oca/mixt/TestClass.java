package old_oca.mixt;

class TestClass {
	
	/*
All the wrapper objects are immutable. When you do i++, what actually happens is
something like this:
i = new Integer( i.intValue() + 1); As you can see, a new Integer object is
assigned back to i.
However, to save on memory, Java 'reuses' all the wrapper objects whose values fall
in the following ranges:
All Boolean values (true and false)
All Byte values
All Character values from \u0000 to \u007f (i.e. 0 to 127 in decimal)
All Short and Integer values from -128 to 127
// So == will always return true when their primitive values are the same and belong to the above list of values.
Note that the following will not compile though:
Byte b = 1; Integer i = 1;
b == i; //Invalid because both operands are of different class.
	 */
	
	public static void main(String[] args) {
		Integer i = Integer.valueOf(34);
		Integer j = i;
		i--;
		//i++;
		System.out.println(i == j);
		
		short s = 12;
		//short r = 100000000000000000000000;
	}
}
