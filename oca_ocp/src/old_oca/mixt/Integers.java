package old_oca.mixt;

public class Integers {

	public static void main(String[] args) {
		Integer i = 2;
		Object o = new Object();
		//System.out.println(i + o);// operatie nedefinita pt int si obj
		
		
		/*
		Arrays are just like regular Objects and arrays of different types have different
		class names. For example, the class name of an int array is [I and the class name
		for an array of int array is [[I.
		 */
		
		int[] j = new int[3];
		System.out.println(j.getClass().isArray());
	}
	
}
