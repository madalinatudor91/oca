package old_oca.grile.andra.cap2;

public class Numbers {

	public static void main(String[] args) {
		long hexVal = 0x10_BA_75; // merge si la alte baze de numeratie
		long binVal = 0b1_0000_10_11;

		char c = (char) -122; // fara cast nu compileaza
		int i = c; // De ce asa mare? char pe 2B?
		System.out.println(c + " " + i);// + " " => string always?
		System.out.println(c + ' ' + i);
	}

}
