package old_oca.grile.andra.cap1;

import java.util.Arrays;

public class Array {

	public static void main(String[] args) {
		int[] gigi;
		//System.out.println(gigi[1]);
		
		int[] source = {1, 2, 3, 4, 5};
		int[] dest = {-9, -8, -7, -6, -5};

		System.arraycopy(source, 2, dest, 3, 1);
		
		System.out.println("Destination");
		for (int i = 0; i < dest.length; i++) {
			System.out.print(dest[i] + " ");
		};
		
		System.out.println("\nSource");
		for (int i = 0; i < source.length; i++) {
			System.out.print(source[i] + " ");
		}
		
		System.out.println("\nFill");
		Arrays.fill(source, 2);
		for (int i = 0; i < source.length; i++) {
			System.out.print(source[i]);
		}
		
		System.out.println("\nCopy Of Range");
		char[] copyFrom = {'d', 'e', 'c', 'a', 'f', 'f', 'e',
	            'i', 'n', 'a', 't', 'e', 'd'};
	            
        // nu tb initializat
        char[] copyTo = java.util.Arrays.copyOfRange(copyFrom, 2, 9);
        System.out.println(new String(copyTo));
		
	}
	
}

class ArrayCopyOfDemo {
	
	// !! pot avea clasa cu doua main-uri, dar doar una e publica, deci e ok
    public static void main(String[] args) {
        System.out.println("test");
    }
}