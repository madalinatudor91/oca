package old_oca.grile.andra.cap1;

public class Expression {

	static int c;

	public static void main(String[] args) {
		int a = 1;
		int b = 2;
		int c = -1;
		int d = -2;
		
		byte a7 = -0;
		int a8 = -0;

		// val minima char -> 0
        System.out.println("++a + b++ * c - d");
        System.out.println(++a + b++ * c - d);

        int x = 2;
        System.out.println("++x + x++ * x - x"); // 4 + 3 * 3 - 3
        System.out.println(++x + x++ * x - x);
        System.out.println(x);

        int y = 2;
        System.out.println("++y * y");
        System.out.println(++y * y);

        int z = 2;
        System.out.println("z++ * z"); //!!!
        System.out.println(z++ * z);


        //System.out.println(true > false);
		
		// si baza 8?
		
		// The number 26, in decimal
		int decVal = 26;
		//  The number 26, in hexadecimal
		int hexVal = 0x1a;
		// The number 26, in binary
		int binVal = 0b11010;
		
		// A floating-point literal is of type float if it ends with the letter F or f;
		// otherwise its type is double and it can optionally end with the letter D or d.

		//Unicode escape sequences may be used elsewhere in a program (such as in field names, for example),
		// not just in char or String literals.
		//float a\u0089 = 0;
//		int \u00ED\u00ED = 3;
        System.out.println("\u00ED\u00ED");

		System.out.println("\f");// what? form feed = ?
		
	}
	
	void test(int c){
		// nu are treaba cu ce e al clasei
		System.out.println(c);
	}
	
}
