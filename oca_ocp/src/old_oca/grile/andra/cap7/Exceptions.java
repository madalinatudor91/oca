package old_oca.grile.andra.cap7;

public class Exceptions {

	/*
EXAM TIP Watch out for code that returns a value from the catch block and
modifies it in the finally block. If a catch block returns a primitive data
type, the finally block can�t modify the value being returned by it. If a catch
block returns an object, the finally block can modify the value being
returned by it.
	 */
	
}

/*

IndexOutOf-
BoundsException is subclassed by ArrayIndexOutOfBoundsException.
ArrayIndexOutOfBoundsException is thrown when a piece of code tries to access
an array out of its bounds (either an array is accessed at a position less than 0 or at a
position greater than or equal to its length). IndexOutOfBoundsException is thrown
when a piece of code tries to access a list, like an ArrayList, using an illegal index.

*/