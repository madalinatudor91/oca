package old_oca.grile.andra.cap7;

public class NoClassDef {

	/*
What would happen if you failed to set your classpath
and, as a result, the JVM was unable to load
the class that you wanted to access or execute? Or
what happens if you try to run your application
before compiling it? In both these conditions, the
JVM would throw NoClassDefFoundError (
	 */
	
}
