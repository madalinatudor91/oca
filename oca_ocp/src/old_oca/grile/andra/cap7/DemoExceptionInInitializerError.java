package old_oca.grile.andra.cap7;

/*
 The ExceptionInInitializerError error
 is typically thrown by the JVM when a
 static initializer in your code throws any
 type of RuntimeException. Figure 7.22
 shows the class hierarchy of ExceptionIn-
 InitializerError.
 A static initializer block is defined
 using the keyword static, followed by
 curly braces, in a class. This block is
 defined within a class, but not within a
 method. It’s usually used to execute code
 when a class loads for the first time. Runtime exceptions arising from any of the following
 will throw this error:
 ■ Execution of an anonymous static block
 ■ Initialization of a static variable
 ■ Execution of a static method (called from either of the previous two items)
 The static initializer block of the class defined in the following example will throw a
 NumberFormatException, and when the JVM tries to load this class, it’ll throw an
 ExceptionInInitializerError:
 */

/*
The order of initialization of a class is:
1. All static constants, variables and blocks.(Among themselves the order is the order
in which they appear in the code.)
2. All non static constants, variables and blocks.(Among themselves the order is the
order in which they appear in the code.)
3. Constructor
 */

public class DemoExceptionInInitializerError {
	static {
		int num = Integer.parseInt("sd", 16);
	}
}