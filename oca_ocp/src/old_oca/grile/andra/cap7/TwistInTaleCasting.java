package old_oca.grile.andra.cap7;

class Ink {
}

interface Printable {
	
	int num = Integer.parseInt("sd", 16);
    Integer ii2 = Integer.valueOf(3);
    Integer ii3 = Integer.valueOf("56");

}

class ColorInk extends Ink implements Printable {
}

class BlackInk extends Ink {
}

class TwistInTaleCasting {
	public static void main(String args[]) {
		Printable printable = null;
		BlackInk blackInk = new BlackInk();
		printable = (Printable) blackInk;
	}
}
/*
a printable = (Printable)blackInk will throw compilation error.
b printable = (Printable)blackInk will throw runtime exception.
c printable = (Printable)blackInk will throw checked exception.
d The following line of code will fail to compile:
printable = blackInk;
*/