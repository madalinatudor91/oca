package old_oca.grile.andra.cap6;

public interface Interface {

	void print();

}

interface Employeee {
}

interface Printable extends Employeee {
	String print();
}

class Programmer {
	String print() {
		return ("Programmer - Mala Gupta");
	}
}

class Author extends Programmer implements Printable, Employeee {
	
	// nu merge fara public!!
	public String print() {
		return ("Author - Mala Gupta");
	}
}
