package old_oca.grile.andra.cap6;

class Satellite {
	void orbit() {
	}
}

class Moon extends Satellite {
	void orbit() {
	}
}

class ArtificialSatellite extends Satellite {
	void orbit() {
	}
}

/*
a The method orbit defined in the classes Satellite, Moon, and Artificial-
Satellite is polymorphic.
b Only the method orbit defined in the classes Satellite and Artificial-
Satellite is polymorphic.
c Only the method orbit defined in the class ArtificialSatellite is polymorphic.
d None of the above.

Answer: a
Explanation: All of these options define classes. When methods with the same method
signature are defined in classes that share an inheritance relationship, the methods
are considered polymorphic.
*/