package old_oca.grile.andra.cap6;

class Person {
}

class Employee extends Person {
}

class Doctor extends Person {
}
// :)) polimorfism doar cand vbim de niste metode
/*
a The code exhibits polymorphism with classes.
b The code exhibits polymorphism with interfaces.
c The code exhibits polymorphism with classes and interfaces.
d None of the above.

Answer: d
Explanation: The given code does not define any method in the class Person that is
redefined or implemented in the classes Employee and Doctor. Though the classes
Employee and Doctor extend the class Person, all these three polymorphism concepts
or design principles are based on a method, which is missing in these classes.*/