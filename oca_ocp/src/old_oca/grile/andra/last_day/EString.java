package old_oca.grile.andra.last_day;

public class EString {

	/*
	 * Explanation: When multiple methods are chained on a single code
	 * statement, the methods execute from left to right, not from right to
	 * left. eVal.indexOf("0") returns a negative value because, as you can see,
	 * the String eVal doesn’t contain the digit 0. Hence, eVal.substring is
	 * passed a negative end value, which results in a RuntimeException
	 */

	public static void main(String args[]) {
		String eVal = "123456789";
		System.out.println(eVal.substring(eVal.indexOf("2"), eVal.indexOf("0"))
				.concat("0"));
	}
}