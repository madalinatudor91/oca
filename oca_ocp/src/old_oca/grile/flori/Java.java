package old_oca.grile.flori;

public class Java {
/* A B
	Q 8 : Consider the code
	package mypackage;

	public class ClassA {
        public static void main(String [] args) {
            new ClassA().doThis();
        }
        public void doThis() {
            System.out.println("Running the ClassA class.");
        }
	}
	Assume that the ClassA.java is in the current directory. And, all the Java commands are run from the current directory. Which of the following are correct:
	a) The command �javac -d . ClassA.java� at command prompt creates a directory called mypackage, and ClassA.class file within that directory.
	b) The above class can be run at the command prompt using the command �java mypackage.ClassA�.
	c) The above class can be run at the command prompt using the command �java ClassA�.
	d) The above class can be run at the command prompt using the command �java ClassA.class�.
	*/
	
	
	
	public static void main(String[] args) {
		StringBuffer sb1 = new StringBuffer("g");
		StringBuffer sb2 = new StringBuffer("g");
		
		StringBuilder sbb1 = new StringBuilder("g");
		StringBuilder sbb2 = new StringBuilder("g");

        // equals nesuprascris in StringBuilder si StringBuffer
		// StringBuffer equals() is not overridden; it doesn't compare values.
		if(sb1.equals(sb2)){
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		
		// aparent la fel si la string builder, equals-ul lor ne directioneaza in object
		if(sbb1.equals(sbb2)){
			System.out.println(true);
		} else {
			System.out.println(false);
		}
	}
	
	
	
	
	
	
	
}
