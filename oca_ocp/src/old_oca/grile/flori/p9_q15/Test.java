package old_oca.grile.flori.p9_q15;

public enum Test {

	// neaparat primele, altfel nu compileaza
	BREAKFAST(7, 30), LUNCH(12, 15), DINNER(19, 45);
	
	private int hh;
	private int mm;
	
	Test(int hh, int mm){
		assert(hh >= 0 && hh <= 23) : "Illegal hours";
		assert(mm >= 0 && hh <= 59) : "Illegal mins";
		this.setHh(hh);
		this.setMm(mm);
	}

	public int getHh() {
		return hh;
	}

	public void setHh(int hh) {
		this.hh = hh;
	}

	public int getMm() {
		return mm;
	}

	public void setMm(int mm) {
		this.mm = mm;
	}
	
	public static void main(String[] args) {
		//Test t = new BREAKFAST;
		Test t = BREAKFAST;
		System.out.println(t.getHh() + " " + t.getMm());
	}
	
}
