package old_oca.grile.flori.p3_q4;

public class Outer {

	private int a = 7;

	class Inner {
		public void displayValue() {
			System.out.println("Value of a is " + a);
		}
	}
}
