package old_oca.grile.virgil.test5.programming_interface;

public interface Worker {

	void performWork();

}

class FastWorker implements Worker {
	public void performWork() {
	}

	// You are creating a class that follows "program to an interface" principle. Which of the
	// following line of code will you most likely be using?
	public Worker getWorker() {
		return new FastWorker();
	}
}