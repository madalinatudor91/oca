package old_oca.grile.virgil.test5;

public class Override {

}

class A {
	public static void sM1() {
		System.out.println("In base static");
	}
}

class B extends A {

//	public static void sM1() {
//		System.out.println("In sub static");
//	}

//	public void sM1() {
//		System.out.println("nu compileaza");
//	}

	public static void main(String[] args) {
		
		// static method cannot be overridden by a non-static method and vice versa
		sM1();
	}
	
}

/*
 Another concept (although not related to this question but about static methods) is that
static methods are never overridden. They are HIDDEN or SHADOWED just like
static or non-static fields. For example,
 */
class C {
	int i = 10;

	public static void m1() {
	}

	public void m2() {
	}
}

// Here, UNLIKE m2, m1() of B does not override m1() of A, it just shadows it, as
// proven by the following code:
class D extends C {
	int i = 20;

	public static void m1() {
	}

	public void m2() {
	}
}