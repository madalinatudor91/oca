package old_oca.grile.virgil.last_day;

public class Equals {

	public static void main(String[] args) {
		// First the value of 'str1' is evaluated (i.e. one). Now, before the method is called, the
		// operands are evaluated, so str1 becomes "two". so "one".equals("two") is false.

		String str1 = "one";
		String str2 = "two";
		System.out.println(str1.equals(str1 = str2));

		System.out.println("String".replace('g', 'G') == "String".replace('g', 'G'));
		System.out.println("String".replace('g', 'g') == new String("String").replace('g', 'g'));
		// replace returns the same object if there is no change.
		System.out.println("String".replace('g', 'g') == "String");
		System.out.println();

	}

}
