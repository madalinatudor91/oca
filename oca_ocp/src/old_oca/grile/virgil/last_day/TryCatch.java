package old_oca.grile.virgil.last_day;

public class TryCatch {

	public static int main() {
		
		// basically, an unreachable statement cause compilation error, expect if(false){} statement, which is valid
		
		
		try {
			System.exit(0);
		} catch (Exception e) {

		} finally{
			// hmm, nu se prinde ca nu returneaza de fapt?
			return 0;
		}
		
	}
	
	public static void main(String[] args) {
		System.out.println(main());
		System.out.println("Ceva la care nu se ajunge :D");
	}
	
}
