package old_oca.grile.virgil.last_day;

public class Overload {

	public void method(Object o) {
		System.out.println("Object Version");
	}

	public void method(java.io.FileNotFoundException s) {
		System.out.println("java.io.FileNotFoundException Version");
	}

	public void method(java.io.IOException s) {
		System.out.println("IOException Version");
	}

    // se duce pe cea mai putin generala
	public static void main(String args[]) {
		Overload tc = new Overload();
		tc.method(null);
	}

}
