package old_oca.grile.virgil.set1;

public class Exceptions {

	/*
	 * 5. IllegalArgumentException extends RuntimeException: Usually thrown by the application if a parameter passed to
	 * a method is not valid.
	 * 6. IllegalStateException extends RuntimeException: Usually thrown by the application.
	 * Signals that a method has been invoked at an illegal or inappropriate time. In other words, the Java environment
	 * or Java application is not in an appropriate state for the requested operation.
	 */

	/*
	 * When an error occurs within a method, the method creates an object and hands it off to the runtime system. The
	 * object, called an exception object, contains information about the error, including its type and the state of the
	 * program when the error occurred. Creating an exception object and handing it to the runtime system is called
	 * throwing an exception.
	 */

	// ce metode are Throwable?
	public static void main(String[] args) {

		try {

		} finally {

		}
		// Compilation fails
		// catch (Exception e) {
		//
		// }

	}

}
