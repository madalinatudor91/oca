package old_oca.grile.virgil.set1;

public class TestClass2 {
	public static int m1(int i) {
		return ++i;
	}

	public static void main(String[] args) {
		int k = 3;
		k += 3 + ++k; // k = k + 3 + 4 => k = 4 + 3 + 4
		System.out.println(k);
	}
}