package old_oca.grile.virgil.set1;

public class TestClass {
	public static void main(String[] args) {
		try {
			doTest();
		} catch (MyException2 me) {
			System.out.println(me);
		}
	}

	static void doTest() throws MyException2 {
		int[] array = new int[10];
		array[10] = 1000;
		doAnotherTest();
	}

	static void doAnotherTest() throws MyException2 {
		throw new MyException2("Exception from doAnotherTest");
	}
}

class MyException2 extends Exception {
	public MyException2(String msg) {
		super(msg);
	}
}

/*

Select 1 option
A. Exception in thread "main"
java.lang.ArrayIndexOutOfBoundsException: 10
at exceptions.TestClass.doTest(TestClass.java:24)
at exceptions.TestClass.main(TestClass.java:14)

B. Error in thread "main" java.lang.ArrayIndexOutOfBoundsException

C. exceptions.MyException: Exception from doAnotherTest

D. exceptions.MyException: Exception from doAnotherTest
at exceptions.TestClass.doAnotherTest(TestClass.java:29)
at exceptions.TestClass.doTest(TestClass.java:25)
at exceptions.TestClass.main(TestClass.java:14)

*/