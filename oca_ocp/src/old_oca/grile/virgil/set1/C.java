package old_oca.grile.virgil.set1;

interface I {
}

class A implements I {
}

class B extends A {
}

class C extends B {

	public static void main(String[] args) {
		A a = new A();
		B b = new B();

		/*
		 class B does implement I because it extends A, which implements I. A reference
		of type I can be cast to any class at compile time. Since B is-a A, it can be
		assigned to a.
		*/
		a = (B) (I) b;
		
		b = (B) (I) a;
		// a = (I) b;
//		I i = (C) a;
	}

}