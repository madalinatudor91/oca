package old_oca.grile.virgil.set1;

public class StringStringBuilder {


	public static void main(String[] args) {
		/*
		Note that the method substring() in StringBuilder/StringBuffer returns a
		String (and not a reference to itself, unlike append, insert, and delete). So
		another StringBuilder method cannot be chained to it. For example, the following is
		not valid: sb.append("a").substring(0, 4).insert(2, "asdf");
		*/
		
		StringBuilder b1 = new StringBuilder("snorkler");
		StringBuilder b2 = new StringBuilder("yoodler");
		
		// a
//		b1.append(b2.substring(2, 5).toUpperCase());
		
		// b
//		b2.insert(3, b1.append("a"));
		
		// c
//	    b1.replace(3, 4, b2.substring(4)).append(b2.append(false));
	    
		System.out.println(b1);
		System.out.println(b2);
	}
	
}
