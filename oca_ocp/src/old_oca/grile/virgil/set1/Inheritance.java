package old_oca.grile.virgil.set1;

public class Inheritance {

	void test() throws Exception {
		System.out.println("base");
	}

	public static void main(String[] args) {
		Inheritance i = new Extends();
		try {

			// in mod normal executa metoda din Extends, dar pentru ca la
			// compilare el o simte pe cea din Inheritance, tb try catch
			i.test();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class Extends extends Inheritance {

	void test() {
		System.out.println("child");
	}

}
