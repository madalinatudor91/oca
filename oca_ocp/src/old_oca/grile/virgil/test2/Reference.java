package old_oca.grile.virgil.test2;

import java.util.ArrayList;
import java.util.List;

public class Reference {

	/*
Since you are doing new ArrayList, you are creating an object of class
ArrayList. You are assigning this object to variable "students", which is
declared of class List. Reference type means the declared type of the variable.
	 */
	List students = new ArrayList();
	
}
