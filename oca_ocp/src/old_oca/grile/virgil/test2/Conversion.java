package old_oca.grile.virgil.test2;

//Actually it prints -46. This is because the information was lost during the conversion
//from type int to type float as values of type float are not precise to nine significant
//digits

public class Conversion {
	public static void main(String[] args) {
		int i = 1234567890;
		float f = i;
		System.out.println(i - (int) f);
		
		
		final short s = 1; 
		// fara final n-ar merge
		byte b = s;
		
		// Integer 0 or 1, 2 etc. is not same as char '0', '1' or '2' etc.
	}
}