package old_oca.grile.virgil.test2;

public class Equals {

    public static void main(String[] args) {
        // Signature of equals method is : boolean equals(Object o); So it can take any
//		object.
//		The equals methods of all wrapper classes first check if the two object are of same
//		class or not. If not, they immediately return false. Hence it will print not equal.
//		
        if (new R().equals(new P())) {
            System.out.println("nada");
        }
    }
}

class P {

}

class R {

}