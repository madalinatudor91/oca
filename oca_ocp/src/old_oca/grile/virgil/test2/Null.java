package old_oca.grile.virgil.test2;

public class Null {

	// daca e setata null, e eligibila pt gc
	Inspiratie i;
	
	void reset(Inspiratie i){
		i = null;
		System.out.println(i);
	}
	
	public static void main(String[] args) {
		Inspiratie j = new Inspiratie();
		System.out.println(j);
		new Null().reset(j);
		System.out.println(j);
	}
	
}
class Inspiratie {
	
}