package old_oca.grile.virgil.test2;

//   cast from char to short and viceversa
/*
Not all short values are valid char values, and neither are all char values valid short
values, therefore compiler complains for both the lines 2 and 3. They will require an
explicit cast.
 */
class Test {
	public static void main(String[] args) {
		short s = 10;
		// la ambele tb cast
		//char c = s;
		//s = c;
	}
}
