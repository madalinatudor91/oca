package old_oca.grile.virgil.test2;

/* //
 A class or interface type T will be initialized at its first active use, which occurs if:
T is a class and a method actually declared in T (rather than inherited from a
superclass) is invoked.
T is a class and a constructor for class T is invoked, or U is an array with element
type T, and an array of type U is created.
A non-constant field declared in T (rather than inherited from a superclass or
superinterface) is used or assigned.
 Java specifies that a reference to a constant field must be
resolved at compile time to a copy of the compile-time constant value, so uses of such
a field are never active uses.
All other uses of a type are passive
A reference to a field is an active use of only the class or interface that actually
declares it, even though it might be referred to through the name of a subclass, a
subinterface, or a class that implements an interface.
 */

class Super {
	static String ID = "QBANK";

    static {
        System.out.print("In super");
    }
}

class Sub extends Super {
	static {
		System.out.print("In Sub");
	}
}

public class Test2 {
	public static void main(String[] args) {
		System.out.println(Sub.ID);
	}
}
