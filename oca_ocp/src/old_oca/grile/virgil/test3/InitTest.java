package old_oca.grile.virgil.test3;

public class InitTest {
	static int si = 10;
	int i;

	/*
	 * A final variable must be initialized when an instance is constructed, or else the code will not compile. This can
	 * be done either in an instance initializer or in EVERY constructor.
	 */
	final boolean bool;
	// 1

	{
		bool = (si > 5);
		i = 1000;
	}

}
