package old_oca.grile.virgil.test3;

public class Strings {

	public static void main(String[] args) {
		
		/*
		 Notice that the Strings differ at the first position. The value returned by
		compareTo is (Unicode value of the left hand side - Unicode value of the right
		hand side).
		Although not required for the exam, it is good to know that for English
		alphabets, the unicode value of any lower case letter is always 32 more than the
		unicode value of the same letter in upper case. So, 'a' - 'A' or 'h' - 'H' is 32.
		 */
		
		System.out.println("hello world".equals("hello world"));
		// equalsIgnoreCase() method treats both cases (upper and lower) as same.
		System.out.println("HELLO world".equalsIgnoreCase("hello world"));
		System.out.println("hello".concat(" world").trim().equals("hello world"));
		System.out.println("hello world".compareTo("Hello world") < 0);
		System.out.println("Hello world".toLowerCase( ).equals("hello world"));
		
	}
	
}
