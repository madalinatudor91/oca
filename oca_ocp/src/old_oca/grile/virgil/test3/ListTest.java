package old_oca.grile.virgil.test3;

import java.util.Iterator;
import java.util.List;

public class ListTest {
	
	public static void main(String args[]) {
		List s1 = new java.util.ArrayList();
		s1.add("a");
		s1.add("b");
		s1.add(1, "c");
		
		// Dragut
		System.out.println(s1);
		for (Object object : s1) {
			System.out.println((String)object);
		}
		
		List s2 = new java.util.ArrayList(s1.subList(1, 1));
        System.out.println(s2.size());
        s1.addAll(s2);
		System.out.println(s1);
	}
}
