package old_oca.grile.virgil.test3;

/*
	 A class or interface type T will be initialized immediately before
	the first occurrence of any one of the following:
	T is a class and an instance of T is created.
	T is a class and a static method declared by T is invoked.
	A static field declared by T is assigned.
	A static field declared by T is used and the field is not a constant variable.
	T is a top-level class, and an assert statement lexically nested within T is executed.
	The statement One o = null; does not fall in either of the cases mentioned above. So
	class One is not initialized and its static block is not executed. Class Two is
	initialized only after its superclass Super has been initialized.
 */
public class Test2StaticBlocks {

	static {
		System.out.println("bloc static");
	}
	
	{
		System.out.println("Bloc nestatic");
	}
	
	static String s = "membru static";
	
	
	public static void main(String[] args) {
		System.out.println("----");
		System.out.println(Test2StaticBlocks.s);
	
		System.out.println("----");
		Test2StaticBlocks t = null;
		
		System.out.println("----");
		Test2StaticBlocks t2 = new Test2StaticBlocks();
	}
	
}
