package old_oca.grile.virgil.test3;

public class NullException {


    public static void main(String args[]) {
        try {
            /*
			 	A NullPointerException will be thrown if the expression given to the throw
				statement results in a null pointer.
			 */

            RuntimeException re = null;
            throw re;
        } catch (Exception e) {
            // Ajunge aici
            System.out.println(e);
        }
    }

}
