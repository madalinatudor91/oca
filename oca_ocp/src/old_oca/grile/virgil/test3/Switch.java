package old_oca.grile.virgil.test3;

public class Switch {
	// Pot avea si aici
	enum Season { SUMMER, WINTER, SPRING, FALL }
	
	public static void main(String[] args) {
		Season s = Season.SPRING;
		switch(s){
		case SUMMER : System.out.println("SUMMER");
		// e default, nu case default
		//case default : System.out.println("SEASON");
		case WINTER : System.out.println("WINTER");
		}
	}
	
}

// si aici, dar il ia pe cel de sus
enum Season { SUMMER, WINTER, SPRING, FALL }