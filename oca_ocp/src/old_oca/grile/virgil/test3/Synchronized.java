package old_oca.grile.virgil.test3;

public class Synchronized {

	// synchronized int k;
//    synchronized static Object l;
//    synchronized Object k;

	native void format();

	static volatile int sa;

//    volatile void test();

    /*

    In other words, the main differences between synchronized and volatile are:

a primitive variable may be declared volatile (whereas you can't synchronize on a primitive with synchronized);
an access to a volatile variable never has the potential to block: we're only ever doing a simple read or write,
so unlike a synchronized block we will never hold on to any lock;
because accessing a volatile variable never holds a lock, it is not suitable for cases where we want
to read-update-write as an atomic operation (unless we're prepared to "miss an update");
a volatile variable that is an object reference may be null (because you're effectively synchronizing on the reference, not the actual object).
Attempting to synchronize on a null object will throw a NullPointerException.
     */
}
