package old_oca.grile.virgil.test3;

public class BlockQ78 {

    public static void main(String[] args) {

        // break intr-un block
        block:
        {
            System.out.println("test");
            break block;
//            System.out.println();
        }

    }

    	/*
(T) A. break without a label, can occur only in a switch, while, do, or for statement.
(F) B. continue without a label, can occur only in a switch, while, do, or for statement.
It cannot occur in a switch.
	 */
}
