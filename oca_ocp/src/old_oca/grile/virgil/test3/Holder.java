package old_oca.grile.virgil.test3;

class Holder {
	int value = 1;
	Holder link;

	public Holder(int val) {
		this.value = val;
	}

	public static void main(String[] args) {
		final Holder a = new Holder(5);
		Holder b = new Holder(10);
		a.link = b;
		b.link = setIt(a, b);
		System.out.println(a.link.value + " " + b.link.value);// nu inteleg
	}

	public static Holder setIt(final Holder x, final Holder y) {
		x.link = y.link;
		// doar aici nu merge pt ca e final, dar continutul poate fi schimbat
		//x = new Holder(5);
		return x;
	}
}