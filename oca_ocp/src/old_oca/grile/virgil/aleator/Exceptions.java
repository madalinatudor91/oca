package old_oca.grile.virgil.aleator;

import java.io.IOException;

public class Exceptions {

	public static void main(String[] args) {
		try {
			
		} catch (Exception e) {
			e = new Exception();
		}
		
		try {
			throw new IOException();
		} catch (NumberFormatException | IOException e) {
			/*
					Invalid because when you catch multiple exceptions
					in the same catch, e is implicitly final
			*/
			// e = new Exception();
		}
	}
	
}
