package old_oca.grile.site;

class Alpha {
	String getType() {
		return "alpha";
	}
}

class Beta extends Alpha {
	String getType() {
		return "beta";
	}
}

// !!! nu il deranjeaza ca nu are public, dar clasa tb sa aiba denumirea lui pentru ca are main-ul
class Gamma extends Beta {
	String getType() {
		return "gamma";
	}

	public static void main(String[] args) {
		
		// !!! asa nu se poate
		// Gamma g1 = new Alpha();
		// Gamma g2 = new Beta();
		// echivalent cu
		// Gamma g4 = new Object(); // cast to Gamma
		/*
		Alpha
		|
		Beta
		|
		Gamma
		*/
		
		
		// asa se poate
		Alpha a = new Gamma();
		Alpha b = new Beta();
		
		
		Alpha g3 = (Alpha) new Alpha();
		
		
		// System.out.println(g1.getType() + " " + g2.getType());
	}
}