package old_oca.grile.site;

class Feline {
	public String type = "f ";

	public short x = 0;
	
	public Feline() {
		System.out.print("feline ");
	}
}

public class Cougar extends Feline {
	public Cougar() {
		System.out.print("cougar ");
	}

	// !!! se apeleaza ambii constructori
	public static void main(String[] args) {
		new Cougar().go();
	}

	// !!! nu se mai cunoaste type din super
	void go() {
		type = "c ";
		System.out.print(this.type + super.type);
	}
}