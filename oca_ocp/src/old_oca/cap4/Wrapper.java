package old_oca.cap4;

public class Wrapper {

	public static void main(String[] args) {
		
		/*
		All of the wrapper classes except Character provide two constructors: one that takes
		a primitive of the type being constructed, and one that takes a String representation
		of the type being constructed
		*/
		Integer i = new Integer(3) + new Integer("4") + new Integer('3') + new Integer('o');
		Character c = new Character('a');
	}
	
}
