package old_oca.cap4;

public class Initializations {

	public static void main(String[] args) {
		FlowerPower f = new FlowerPower();
		System.out.println(f.petaleNumber);
		System.out.println(f.isSmelling);
		System.out.println(f.firstLetterName + "-");
		System.out.println(f.gramaj);
		System.out.println(f.gramajD);
		System.out.println(f.integer);
		
		int c; // nu e initializat, se prinde si crapa
		//System.out.println(c);
		float g = (float) 5;
		
		//char z = '/u-0'; // invalid character constant
		
		
		Integer z = 5;
		int zz = z;
		
		float fl = 555_555;
		float f2 = 0.4_444f;
		
		//int aa = 4.5 / 1.5;
		//int aa = (int) 4.5 / 1.5;
		int aa = (int) 4.5 / (int)1.5;
		int bbi = (int) (4.5 / 1.5);
		System.out.println(aa);
		
		float bb = (float) (4.5 / 1.5 * 2);
		System.out.println(bb);
	}
	
}

class FlowerPower{
	
	float gramaj;
	
	double gramajD;
	
	int petaleNumber;
	
	boolean isSmelling;
	
	char firstLetterName;
	
	Integer integer;
	
}
