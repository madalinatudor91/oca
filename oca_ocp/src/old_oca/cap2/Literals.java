package old_oca.cap2;

public class Literals {

    public static void main(String[] args) {
        /*
Although strings are not primitives, they're included in this section because they
can be represented as literals�in other words, typed directly into code. The only
other nonprimitive type that has a literal representation is an array, which we'll look
at later in the chapter.
		 */

        //The following is legal,
        byte b = 27;
        // but only because the compiler automatically narrows the literal value to a byte. In
        // other words, the compiler puts in the cast.

        // explicit cast, se intampla automat
        byte c = 3;

        int by = b + c; // rezultatul e int
        byte byy = (byte) (b + c); // tb cast

		/*
Take our word for it; there are 32 bits there.
To narrow the 32 bits representing 128, Java simply lops off the leftmost (higherorder)
24 bits. We're left with just the 10000000. But remember that a byte is
signed, with the leftmost bit representing the sign (and not part of the value of the
variable). So we end up with a negative number (the 1 that used to represent 128
now represents the negative sign bit). Remember, to find out the value of a negative
number using two's complement notation, you flip all of the bits and then add 1.
Flipping the 8 bits gives us 01111111, and adding 1 to that gives us 10000000, or
back to 128! And when we apply the sign bit, we end up with �128.
		 */
        byte j = (byte) 9999999;
        System.out.println(j);


    }

}
