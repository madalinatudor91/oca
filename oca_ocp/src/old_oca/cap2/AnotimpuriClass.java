package old_oca.cap2;

enum Anotimpuri {

	IARNA, VARA
	
}

public class AnotimpuriClass{
	
	public static void main(String[] args) {
		// si enumurile, si interfetele sunt obiecte
		
		System.out.println(Anotimpuri.class);
		System.out.println(Anotimpuri.IARNA.equals(Anotimpuri.VARA));
		
		if (Anotimpuri.IARNA instanceof Object){
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		
		if(new I(){} instanceof Object){
			System.out.println(true);
		} else {
			System.out.println(false);
		}
		
		new I(){}.toString();
		Anotimpuri.VARA.toString();
	}
	
}

interface I {
	
}