package old_oca.cap2.statements;

public class BreakContinueTest {

	public static void main(String[] args) {

		System.out.println("--break--");

		for (int i = 0; i < 13; i++) {
			System.out.println("inainte " + i);
			if (i % 2 == 0) {
				break;
			}
			System.out.println("dupa " + i);
		}

		System.out.println("--continue--");

		for (int i = 0; i < 13; i++) {
			System.out.println("inainte " + i);
			if (i % 2 == 0) {
				// de fapt sare la urmatoarea iteratie, fara sa execute tot, un fel de break, dar la nivel de iteratie
				// curenta, nu pe for
				continue;
			}
			System.out.println("dupa " + i);
		}
	}

}
