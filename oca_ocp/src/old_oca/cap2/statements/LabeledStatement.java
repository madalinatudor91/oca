package old_oca.cap2.statements;

public class LabeledStatement {

    public static void main(String[] args) {
        int i = 0;
        while (i < 5) {
            System.out.println("While 1");

            myBreakLabel:
            while (true) {
                System.out.println("While 2");
                while (true) {
                    System.out.println("while 3");
                    break myBreakLabel;
                }
            }
            System.out.println(i);
            i++;

        }


        label2:
        if (1 == 1) {
            System.out.println("Something");
            // break; // cannot be used outside of a loop or a switch, ce tare
        }

    }

}
