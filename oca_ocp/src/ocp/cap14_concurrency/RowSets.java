package ocp.cap14_concurrency;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;
import java.sql.SQLException;

public class RowSets {

    public static void main(String[] args) throws SQLException {
        RowSetFactory rsf = RowSetProvider.newFactory();
        CachedRowSet studentRS = rsf.createCachedRowSet();
        studentRS.setCommand("select SID, NAME from STUDENT2");
//        studentRS.execute(c); //at this points studentRS contains the data returned by the query
    }
}
