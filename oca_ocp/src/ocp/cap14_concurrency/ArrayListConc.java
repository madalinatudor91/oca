package ocp.cap14_concurrency;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class ArrayListConc {


    /*
    A read lock can be locked by multiple threads simultaneously (by calling lock.readLock().lock() ), if the write lock is free.
     If the write lock is not free, a read lock cannot be locked. The write lock can be locked (by calling lock.writeLock().lock() )
      only by only one thread and only when no thread already has a read lock or the write lock.
      In other words, if one thread is reading, other threads can read, but no thread can write.
      If one thread is writing, no other thread can read or write.
     */

    private List<Integer> integers = new ArrayList<>();
    private ReentrantReadWriteLock rw = new ReentrantReadWriteLock(); // nice feature

    public void add(Integer i) {
        rw.writeLock().lock(); // nici citire, nici scriere
        try {
            integers.add(i); // nu cere vreo exceptie
        } finally {
            rw.writeLock().unlock();
        }

        try {
            System.out.println("gigi");
        } finally {
            // merge orice in try/ catch?
        }
    }

    public int findMax() {
        rw.readLock().lock();
        try {
            return Collections.max(integers);
        } finally {
            rw.readLock().unlock();
        }
    }
}
