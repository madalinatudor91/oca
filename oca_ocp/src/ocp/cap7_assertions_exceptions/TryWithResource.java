package ocp.cap7_assertions_exceptions;


class One implements AutoCloseable {

    @Override
    public void close() {
        System.out.println("Close One");
    }
}

class Two implements AutoCloseable {

    @Override
    public void close() throws Exception { // din cauza astuia e nevoie de catch Exception
        System.out.println("Close Two");
    }
}

public class TryWithResource {

    public static void main(String[] args) {
        System.out.println("Test 1\n");
        test1();
        System.out.println("\nTest 2\n");
        test2(); // intra intai in finally? da, pt ca sunt cd ca fiind parte din blocul try...
    }

    private static void test1() {
        try (One one = null; Two two = new Two()){
            System.out.println("Try block");
        } catch (Exception e) {
            System.out.println("Catch Exception");
        } finally {
            System.out.println("Finally block");
        }
    }

    private static void test2() {
        try (One one = new One(); Two two = new Two()){ // intai 2, apoi 1
            System.out.println("Try block");
            throw new IllegalArgumentException();
        } catch (IllegalArgumentException e) {
            System.out.println("Catch IllegalArgumentException");
        } catch (Exception e) {
            System.out.println("Catch Exception");
        } finally {
            System.out.println("Finally block");
        }
    }

}
