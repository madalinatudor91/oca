package ocp.cap7_assertions_exceptions;

import java.io.Closeable;
import java.io.IOException;

public class TryWithResAutoCloseable {
}

// Closeable extends Autocloseable
class Animals {
    class Lamb implements Closeable {
        public void close() {
            throw new RuntimeException("a");
        }
    }

    public static void main(String[] args) {
        new Animals().run();
    }

    public void run() {
        try (Lamb l = new Lamb(); /*String z = test(); Bla b = new Bla();*/) {
            throw new IOException();
        } catch (Exception e) {
            throw new RuntimeException("c");
        }
    }

    private static String test() {
        return "gigel";
    }
}

class Bla {
}

// ILLEGAL � Closeable only allows IOExceptions or subclasses
class D implements Closeable {

    public void close() {//throws Exception {

    }
}