package ocp.cap7_assertions_exceptions;

import java.io.IOException;
import java.sql.SQLException;

public class MultipleCatch {

    public static void main(String[] args) {

        try {

        } catch (ArrayIndexOutOfBoundsException /*e1*/ | NullPointerException e2) {

        }

//        try {
//            // types in multi-catch must be disjoint: Exception is a subclass of Exception
//        } catch (Exception | Exception e) {
//
//        }
    }


    public void rethrow() throws SQLException, IOException {
        try {
            couldThrowAnException();
        } catch (Exception e) { // watch out: this isn't really
            // catching all exception subclasses
            System.out.println(e);
            throw e; // note: won't compile in Java 6
            /*
                In jv 7 I'll pretend the developer meant to only catch SQLException and IOException.

                As with multi-catch, you shouldn't be assigning a new value to the catch parameter
            in real life anyway.
                The difference between this and multi-catch is where the
            compiler error occurs. For multi-catch, the compiler error occurs on the line where
            we attempt to assign a new value to the parameter, whereas here, the compiler error
            occurs on the line where we throw e. It is different because code written prior to
            Java 7 still needs to compile. Since the multi-catch syntax is brand new, there is no
            legacy code to worry about.
         */
        }
    }

    private void couldThrowAnException() {

    }

}
