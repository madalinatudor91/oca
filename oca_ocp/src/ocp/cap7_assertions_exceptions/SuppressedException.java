package ocp.cap7_assertions_exceptions;

import java.io.IOException;

public class SuppressedException {

    public static void main(String[] args) {
        try (AutoCloseResource one = new AutoCloseResource(); AutoCloseResource two = new AutoCloseResource()) {
//            throw new Exception("Try");
            // deci close se intampla tot in blocul de try
        } catch (Exception e) {
            System.err.println("Catch " + e.getMessage());
            for (Throwable t : e.getSuppressed()) {
                System.err.println("suppressed:" + t);
            }
        } finally {
            System.out.println("oh, finally..."); // afisat random, ba primul, ba al doilea, ba ultimul...
        }
    }
}

class AutoCloseResource implements AutoCloseable {
    static int i;
    public void close() throws IOException {
        throw new IOException("Closing " + (++i));
    }
}