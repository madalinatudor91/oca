package ocp.cap7_assertions_exceptions;

public class AssertTest {

    public static void main(String[] args) {
        assert (true);

        assert(false); // java.lang.AssertionError

        int x = 9, y = 20; // uitasem de sintaxa asta :D

        boolean b;
        if (b = false) { // uitasem iar...

        }

        while (b = true) {
            // deci aici poate fi pacalit...
        }

        System.out.println("ceva ce oricum nu va fi printat");

        while(true) {

        }
        // System.out.println("nu compileaza");


        /*
        java -ea -dsa Enable assertions in general, but disable assertions in
system classes.
        java -ea -da:com.foo... Enable assertions in general, but disable assertions in
package com.foo and any of its subpackages
         */
    }
}
