package ocp.cap15_jdbc;

import java.sql.*;

public class FirstTest {

    public static void main(String[] args) {
        String url = "jdbc:derby://localhost:1521/BooksDb";
        String user = "looser";
        String pass = "pass";

        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            Statement st = conn.createStatement();
            String sql = "select * from books";
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("id"));
//                rs.getHoldability() // ??
                rs.getInt(1); // col index

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
