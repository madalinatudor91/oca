package ocp.cap8_string_formatting;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormats {

    public static void main(String[] args) {
        float f1 = 123.456789f;
        Locale locRo = new Locale("ro"); // France
        NumberFormat[] nfa = new NumberFormat[7];
        nfa[0] = NumberFormat.getInstance();
        nfa[1] = NumberFormat.getInstance(locRo);
        nfa[2] = NumberFormat.getCurrencyInstance();
        nfa[3] = NumberFormat.getCurrencyInstance(locRo);
        nfa[4] = NumberFormat.getNumberInstance();
        nfa[5] = NumberFormat.getNumberInstance(locRo);
        nfa[6] = DecimalFormat.getInstance();

        for (NumberFormat nf : nfa) {
            System.out.println(nf.format(f1));
        }

        nfa[0].setGroupingUsed(true);
        System.out.println("\n" + nfa[0].format(f1));
        System.out.print(nfa[0].getMaximumFractionDigits() + " ");

        // if less than zero, then zero is used.
        nfa[0].setMaximumFractionDigits(5);
        System.out.println("\n" + nfa[0].format(f1)); // aproximeaza


    }

}
