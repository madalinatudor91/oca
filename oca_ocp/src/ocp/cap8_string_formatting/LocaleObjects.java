package ocp.cap8_string_formatting;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

public class LocaleObjects {

    public static void main(String[] args) {
       // Locale locale = new Locale();
        Locale.getDefault(Locale.Category.DISPLAY); // ce e acest Category?
        System.out.println(Locale.getDefault());

        Locale it = new Locale("it");
        Locale in = new Locale("in");
        Locale ro = new Locale("ro");
        System.out.println("Italia: " + DateFormat.getDateInstance(DateFormat.SHORT, it).format(new Date()));
        System.out.println("India: " + DateFormat.getDateInstance(DateFormat.SHORT, in).format(new Date()));
        System.out.println("Romania: " + DateFormat.getDateInstance(DateFormat.SHORT, ro).format(new Date()));
        System.out.println("Romania: " + DateFormat.getDateInstance(DateFormat.MEDIUM, ro).format(new Date()));
        System.out.println("Romania: " + DateFormat.getDateInstance(DateFormat.FULL, ro).format(new Date()));

        /*
        Remember that both DateFormat and NumberFormat objects can have
their locales set only at the time of instantiation. Watch for code that attempts to change
the locale of an existing instanceóno such methods exist!
         */

    }

}


