package ocp.cap8_string_formatting;

import java.util.Calendar;
import java.util.Locale;

public class GetLocaleCalendar {

    public static void main(String[] args) {
        System.out.println(Locale.getDefault());
        Calendar cal = Calendar.getInstance();
        System.out.println(cal.getFirstDayOfWeek()); // Sunday

        cal.roll(Calendar.DAY_OF_MONTH, 1); // tomorrow, but it won't change the month, if needed
        System.out.println(cal.getTime());
    }

}
