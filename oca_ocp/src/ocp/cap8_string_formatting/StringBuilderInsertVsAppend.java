package ocp.cap8_string_formatting;

public class StringBuilderInsertVsAppend {
    /*
    The append()method appends to the end of the StringBuilder's current
value, and if you append past the current capacity, the capacity is automatically increased. Note:
Invoking insert() past the current capacity will cause an exception to be thrown.
     */
    public static void main(String[] args) {
        StringBuilder sb = new StringBuilder(8);
        System.out.print(sb.length() + " " + sb + " ");
        sb.insert(0, "abcdef");
        sb.append("789");
        System.out.println(sb.length() + " " + sb);
    }
}
