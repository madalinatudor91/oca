package ocp.cap8_string_formatting;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

public class DateFormats {

    public static void main(String[] args) {
        Date date = new Date();
        System.out.println(DateFormat.getInstance().format(date));
        System.out.println(DateFormat.getDateInstance().format(date)); // without time
        System.out.println(DateFormat.getDateInstance(DateFormat.SHORT).format(date));
        System.out.println(DateFormat.getDateInstance(DateFormat.MEDIUM).format(date));
        System.out.println(DateFormat.getDateInstance(DateFormat.LONG).format(date));
        System.out.println(DateFormat.getDateInstance(DateFormat.FULL).format(date));

        try {
            DateFormat.getInstance().parse("Jul 16, 1991"); // can be used ParsePosition, overloaded method
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //DateFormat.getInstance(new Locale("it"));
        DateFormat.getDateInstance(DateFormat.AM_PM_FIELD, new Locale("it"));
        //DateFormat.getDateInstance(new Locale("it"))
    }

}
