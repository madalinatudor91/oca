package ocp.cap8_string_formatting.tokens;

import java.util.Arrays;

public class StringSplit {

    public static void main(String[] args) {
        String s = "ab.cd.ef";
        String[] tokens = s.split(".");
        System.out.println(Arrays.toString(tokens));
    }
}
