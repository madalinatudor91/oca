package ocp.cap8_string_formatting.tokens;

import java.util.Scanner;

public class ScannerFind {

    public static void main(String[] args) {
        callScanner("\\d\\d");
    }

    private static void callScanner(String pattern) {
        Scanner s = new Scanner(System.in); // Scanners can be constructed using files, streams, or strings as a source.

        /*
        The Scanner class has nextXxx() (for instance, nextLong()) and hasNextXxx()
(for instance, hasNextDouble()) methods for every primitive type except char. In
addition, the Scanner class has a useDelimiter() method that allows you to set
the delimiter to be any valid regex expression.
         */

        System.out.flush();
        String token;
        do {
            token = s.findInLine(pattern);
            System.out.println(token);
        } while (token != null);
    }

}
