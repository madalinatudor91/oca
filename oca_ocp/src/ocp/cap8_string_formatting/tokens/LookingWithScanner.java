package ocp.cap8_string_formatting.tokens;

import java.util.Scanner;

public class LookingWithScanner {

    public static void main(String[] args) {
        String input = "1 2 a 3 45 6";
        Scanner sc = new Scanner(input);
        int x = 0;
        do {
            x = sc.nextInt(); // hasNext...
            System.out.print(x + " ");
        } while (x != 0);
    }
}
