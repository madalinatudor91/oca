package ocp.cap8_string_formatting.tokens;

import java.util.StringTokenizer;

public class StringTokenizerTest {

    public static void main(String[] args) {
        StringTokenizer st = new StringTokenizer("bla*bla*bla", "bla");
        System.out.println(st.countTokens());
        while (st.hasMoreElements()) { // totuna ca hasMoreElements, din Enumeration, acum e la moda insa Iterator
            System.out.println(st.nextToken());
        }
    }
}
