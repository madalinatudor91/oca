package ocp.cap8_string_formatting.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexFind {

    public static void main(String[] args) {
        showPattern("aba", "abababa");
        showPattern("\\d", "a12c3e456f"); // \d A digit (0�9)
        showPattern("\\w", "a 1 56 _Z"); // The only characters in this source that don't match the definition of a word
        // character are the whitespaces
        showPattern("\\S", "w1w w$ &#w1");
        showPattern("\\b", "w2w w$ &#w2"); // boundary (inceput-sfarsit)
        showPattern("\\b", "#ab de#");
        showPattern("0[xX]([A-F0-9])+", "0X56548*");
    }

    private static void showPattern(String pattern, String source) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(source);
        System.out.println("\n" + m.pattern());
        while (m.find()) { // In general, a regex search runs from left to right, and once a source's character
            // has been used in a match, it cannot be reused.
            System.out.print(m.start() + " " + m.group());
        }
    }
}
