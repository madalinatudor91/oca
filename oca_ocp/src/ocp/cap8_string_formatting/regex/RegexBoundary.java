package ocp.cap8_string_formatting.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexBoundary {

    public static void main(String [] args) {
        Pattern p = Pattern.compile("\\b");
        Matcher m = p.matcher("^23 *$76 bc"); /* In these
        cases, regex is looking for a specific relationship between two adjacent characters.
                When it finds a match, it returns the position of the second character.
                */
        System.out.println("match positions: ");
        while(m.find()) {
            System.out.println(m.start() + " ");
        }
        System.out.println("");
    }

}
