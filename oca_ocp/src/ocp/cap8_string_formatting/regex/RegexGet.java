package ocp.cap8_string_formatting.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexGet {

    public static void main(String[] args) {
        applyRegex("\\d+", "1 a12 234b");
        applyRegex("\\d*", "ab34df");
    }

    private static void applyRegex(String pattern, String source) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(source);
        System.out.println("\n" + m.pattern());
        while (m.find()) {// Attempts to find the next subsequence of the input sequence that matches the pattern.
            System.out.println(m.start() + " " + m.group());
        }
    }

}
