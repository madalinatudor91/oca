package ocp.cap8_string_formatting.regex;

import java.util.regex.*;

class Quetico {
    public static void main(String[] args) {
        boundaries("\\b");
        boundaries("\\B");
    }

    static void boundaries(String regex) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher("^23 *$76 bc");
        System.out.println("^23 *$76 bc");
        System.out.println("0123456789012");
        while (m.find()) {
            System.out.print(m.start() + " ");
        }
        System.out.println("");
    }
}
