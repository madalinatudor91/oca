package ocp.cap8_string_formatting.formatting;

public class Formatting1 {

    public static void main(String[] args) {
        System.out.printf("%-8d \n", 1584);
//        System.out.printf("%-d", 1584); // java.util.MissingFormatWidthException

        /*
        ? As of Java 5 there are two methods used to format data for output. These
        methods are format() and printf(). These methods are found in the
        PrintStream class, an instance of which is the out in System.out.
        */
        int i1 = -123;
        int i2 = 12345;
        System.out.printf(">%1$(7d< \n", i1);
        System.out.printf(">%0,7d< \n", i2);
        System.out.format(">%+-7d< \n", i2);
        System.out.printf(">%2$b + %1$5d< \n", i1, false);
        System.out.format("%b \n", 123.4); //la boolean nu crapa
        System.out.format("%b \n", 0);

        System.out.printf("%,f", -2222.5);

//        System.out.format("%d", 123.4); // IllegalFormatConversion

        System.out.println("\n\n");
//        System.out.printf("%2$d", 5); // MissingFormatArgumentException
//        System.out.printf("%d"); // MissingFormatArgumentException
        System.out.printf("%+d \n", -524);
        System.out.printf("%07d \n", -64);
        System.out.printf("%,f \n", 9.6767);
        System.out.printf("%2.3f \n", 9.6767);


        System.out.println(5.0f);
        System.out.println(5.0d);
    }
}
