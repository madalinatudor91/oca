package ocp.cap8_string_formatting;

import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleSamples {

    public static void main(String[] args) {
        Locale locale = new Locale("fr", "CA");
        ResourceBundle rb = ResourceBundle.getBundle("RB", locale);

        // RB_fr_CA.java -> .prop -> RB_fr -> RB_[locale]_[country] -> RB_[locale] -> RB -> MissingResourceException. :D
    }

}

class Labels_en_CA extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {
        return new Object[][]{
                {"hello", new StringBuilder("from Java")}
        };
    }
}