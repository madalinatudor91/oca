package ocp.cap10_design_patterns.dao;

import ocp.cap10_design_patterns.entity.Book;

import java.util.Collection;

public interface BookDao {

    Collection<Book> findAllBooks();

    Book findBookByIsbn(Book book);

    void create(Book book);

    void delete(Book book);

    void update(Book book);

//    static void haha();

}
