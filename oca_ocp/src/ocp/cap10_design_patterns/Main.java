package ocp.cap10_design_patterns;

import ocp.cap10_design_patterns.dao.BookDao;
import ocp.cap10_design_patterns.factory.Factory;
import ocp.cap10_design_patterns.factory.FactoryImpl;

public class Main {

    public static void main(String[] args) {
        Factory factory = new FactoryImpl();
        BookDao dao = factory.createDao();
    }

}
