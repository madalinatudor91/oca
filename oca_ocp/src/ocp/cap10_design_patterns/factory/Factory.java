package ocp.cap10_design_patterns.factory;

import ocp.cap10_design_patterns.dao.BookDao;

public abstract class Factory {

    public abstract BookDao createDao();
}
