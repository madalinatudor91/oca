package ocp.cap10_design_patterns.factory;

import ocp.cap10_design_patterns.dao.BookDao;
import ocp.cap10_design_patterns.dao.BookDaoImpl;

import java.io.Serializable;

public class FactoryImpl /* implements Serializable */ extends Factory {

    @Override
    public BookDao createDao() {
        return new BookDaoImpl();
    }
}
