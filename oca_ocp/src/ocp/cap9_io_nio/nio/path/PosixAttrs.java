package ocp.cap9_io_nio.nio.path;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

public class PosixAttrs {

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("/tmp/file2");
        Files.createFile(path);
        PosixFileAttributes posix = Files.readAttributes(path, PosixFileAttributes.class); // get the Posix type
        Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rw-r--r--"); // UNIX style
        Files.setPosixFilePermissions(path, perms); // set permissions, only for posix
        System.out.println(posix.permissions()); // get permissions
    }
}
