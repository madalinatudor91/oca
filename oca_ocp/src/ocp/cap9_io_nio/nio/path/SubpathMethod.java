package ocp.cap9_io_nio.nio.path;

public class SubpathMethod {

    /*
    remember the following points about Path.subpath(int beginIndex, int endIndex)
    1. Indexing starts from 0.
    2. Root (i.e. c:\) is not considered as the beginning.
    3. name at beginIndex is included but name at endIndex is not.
    4. paths do not start or end with \.
     */

}
