package ocp.cap9_io_nio.nio.path;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

public class RemoveClassFiles extends SimpleFileVisitor<Path> {

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (file.getFileName().endsWith(".class")) {
            Files.delete(file); // delete the file
        }
        return FileVisitResult.CONTINUE; // FileVisitResult, see values

        /*
        Note that Java goes down as deep as it can before returning back up the tree. This
is called a depth-first search. We said "might" because files and directories at the same
level can get visited in either order.
         */
    }

    public static void main(String[] args) throws IOException {
        RemoveClassFiles simpleFileVisitor = new RemoveClassFiles();
        Files.walkFileTree(Paths.get("/home/src"), simpleFileVisitor);
    }
}
