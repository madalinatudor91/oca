package ocp.cap9_io_nio.nio.path;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class FileTimeModification {

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("test.pdf");
        Date januaryFirst = new GregorianCalendar(2013, Calendar.JANUARY, 1).getTime();
        FileTime fileTime = FileTime.fromMillis(januaryFirst.getTime()); // FileTime class
        Files.setLastModifiedTime(path, fileTime);
        System.out.println(Files.getLastModifiedTime(path));

        System.out.println(Files.isExecutable(path));
        System.out.println(Files.isReadable(path));
        System.out.println(Files.isWritable(path));

        // Old IO approach

        File file = new File("test");
        file.lastModified();
        file.canRead();
        file.canWrite();
        file.canExecute();
        file.setLastModified(januaryFirst.getTime());

    }
}
