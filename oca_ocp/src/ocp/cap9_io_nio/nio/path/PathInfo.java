package ocp.cap9_io_nio.nio.path;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathInfo {

    public static void main(String[] args) {
        Path path = Paths.get("D:\\work\\disertatie");
        System.out.println(path);

        System.out.println(path.getFileName());
        System.out.println(path.getFileSystem());
        System.out.println(path.getName(0));
        System.out.println(path.getName(1));
//        System.out.println(path.getName(2)); // crapa
        System.out.println(path.getNameCount());
        System.out.println(path.getParent());
        System.out.println(path.getRoot());
        System.out.println(path.subpath(0, 2));
    }
}
