package ocp.cap9_io_nio.nio.path;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.DosFileAttributeView;
import java.nio.file.attribute.DosFileAttributes;

public class DosAttrs {


    public static void main(String[] args) throws IOException {
        Path path= Paths.get("D:/test");
        Files.createFile(path); // create file
        Files.setAttribute(path, "dos:hidden", true); // set attribute
        Files.setAttribute(path, "dos:readonly", true); // another one
        DosFileAttributes dos = Files.readAttributes(path,
                DosFileAttributes.class); // dos attributes
        System.out.println(dos.isHidden());
        System.out.println(dos.isReadOnly());
        Files.setAttribute(path, "dos:hidden", false);// set dos attrs
        Files.setAttribute(path, "dos:readonly", false); // pus ca readOnly => il ignora, nu seteaza nimic

        dos = Files.readAttributes(path,  DosFileAttributes.class); // get attributes again
        System.out.println(dos.isHidden());
        System.out.println(dos.isReadOnly());
        Files.delete(path);

        /*
         The other tricky thing is that you cannot delete a read-only file. That's why the
code calls setAttribute a second time with false as a parameter, to make it no
longer "read only" so the code can clean up after itself.
         */

        DosFileAttributeView view = Files.getFileAttributeView(path,
                DosFileAttributeView.class);
        view.setHidden(true); //  set dos attrs with view
        view.setReadOnly(true);

    }

}
