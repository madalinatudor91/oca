package ocp.cap9_io_nio.nio.path;

public class FilesIsSameFile {

    public static void main(String[] args) {
        /*
        Files.isSameFile method doesn't check the contents of the file.
        It is meant to check if the two path objects resolve to the same file or not.
         */
    }
}
