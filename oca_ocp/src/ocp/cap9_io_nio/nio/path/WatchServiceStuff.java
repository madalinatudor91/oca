package ocp.cap9_io_nio.nio.path;

import java.io.IOException;
import java.nio.file.*;

public class WatchServiceStuff {

    /*
    The basic flow of WatchService stays the same, regardless of what you want to do:
    1. Create a new WatchService
    2. Register it on a Path listening to one or more event types
    3. Loop until you are no longer interested in these events
    4. Get a WatchKey from the WatchService
    5. Call key.pollEvents and do something with the events
    6. Call key.reset to look for more events
     */

    //
    public static void main(String[] args) throws IOException {
        Path dir = Paths.get("/dir");
        WatchService watcher = FileSystems.getDefault().newWatchService();
        /*
        These changes can be made manually by a human or by another program on the computer.
        Renaming a file or directory is interesting, as it does not show up as ENTRY_MODIFY.
From Java's point of view, a rename is equivalent to creating a new file and deleting
the original. This means that two events will trigger for a renameóboth ENTRY_
CREATE and ENTRY_DELETE. Actually editing a file will show up as ENTRY_MODIFY.
         */
        dir.register(watcher, StandardWatchEventKinds.ENTRY_DELETE);
        while (true) {
            WatchKey key;
            try {
                key = watcher.take(); // wait for a deletion
            } catch (InterruptedException e) {
                return;
            }

            for (WatchEvent<?> event : key.pollEvents()) {
                WatchEvent.Kind<?> kind = event.kind();
                System.out.println(kind.name() + " " + kind.type()); // create/delete/modify " " always a Path for us
                System.out.println(event.context()); // name of the file
                if ("gigi".equals(event.context().toString())) {
                    // gata delete-ul
                    return;
                }
            }
            key.reset(); // keep looking for events
        }

    }
}
