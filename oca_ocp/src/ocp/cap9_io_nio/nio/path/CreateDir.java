package ocp.cap9_io_nio.nio.path;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreateDir {

    public static void main(String[] args) throws IOException {
        Path path1 = Paths.get("/java/source");
        Path path2 = Paths.get("/java/source/directory");
        Path file = Paths.get("/java/source/directory/Program.java");
        Files.createDirectory(path1); // create first level of directory
        Files.createDirectory(path2); // create second level of directory
        Files.createFile(file); // create file
//        Or we could create all the directories in one go:
        Files.createDirectories(path2); // create all le
    }
}
