package ocp.cap9_io_nio.nio.path;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathRelativize {

    public static void main(String[] args) {
        /*
        path1.relativize(path2) should be read as "give me a path that shows how to get from path1 to path2."
         */
        Path dir = Paths.get("/home/java");
        Path music = Paths.get("/home/java/country/Swift.mp3");
        Path mp3 = dir.relativize(music); // [cel din care plec].relativize([unde vreau sa ajung], sursa.relativize(destinatie))
        System.out.println(mp3);

        Path absolute1 = Paths.get("/home/java");
        Path absolute2 = Paths.get("/usr/local");
        Path absolute3 = Paths.get("/home/java/temp/music.mp3");
        Path relative1 = Paths.get("temp");
        Path relative2 = Paths.get("temp/music.pdf");
        System.out.println("1: " + absolute1.relativize(absolute3));
        System.out.println("2: " + absolute3.relativize(absolute1));
        System.out.println("3: " + absolute1.relativize(absolute2));
        System.out.println("4: " + relative1.relativize(relative2)); // deci relativ inseamna fara / de la inceput?
        System.out.println("5: " + absolute1.relativize(relative1));//java.lang.IllegalArgumentException: 'other' is different type of Path
    }
}
