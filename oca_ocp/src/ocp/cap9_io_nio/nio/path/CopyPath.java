package ocp.cap9_io_nio.nio.path;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class CopyPath {

    public static void main(String[] args) throws IOException {

        Path one = Paths.get("/temp/test1"); // exists
        Path two = Paths.get("/temp/test2.txt"); // exists
        Path targ = Paths.get("/temp/test23.txt"); // doesn't yet exist
        Files.copy(one, targ); // now two copies of the file
        Files.copy(two, targ); // oops, FileAlreadyExistsException

                /*Java sees it is about to overwrite a file that already exists. Java doesn't want us to lose
the file, so it "asks" if we are sure by throwing an Exception. copy()and move()
actually take an optional third parameterózero or more CopyOptions. The most
useful option you can pass is StandardCopyOption.REPLACE_EXISTING.
         */
        Files.copy(two, targ, StandardCopyOption.REPLACE_EXISTING);
    }

/*
Path copy(Path source, Path target,
CopyOption... options)
Copy the file from source to target and
return target
Path move(Path source, Path target,
CopyOption... options)
Move the file from source to target and
return target
void delete(Path path) Delete the file and throw an Exception
if it does not exist
boolean deleteIfExists(Path path) Delete the file if it exists and return
whether file was deleted
boolean exists(Path path,
LinkOption... options)
Return true if file exists
boolean notExists(Path path,
LinkOption... options)
Return true if file does not exist
 */
}
