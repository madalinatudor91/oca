package ocp.cap9_io_nio.nio.path;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class DirectoryStreamExample {

    public static void main(String[] args) {
        Path dir = Paths.get("/home/users");
        try(DirectoryStream<Path> stream = Files.newDirectoryStream(dir)) {
            for (Path p: stream) {
                System.out.println(p.getFileName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            DirectoryStream<Path> stream = Files.newDirectoryStream( dir, "[vw]*"); // cauta doar in dir curent, dupa cv care incepe cu v sau w
            // pt vazut in subdirectoare se fol FileVisitor
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
