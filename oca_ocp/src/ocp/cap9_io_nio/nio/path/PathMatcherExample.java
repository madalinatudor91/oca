package ocp.cap9_io_nio.nio.path;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;

public class PathMatcherExample {

    public static void main(String[] args) {
        //  SimpleFileVisitor, ca parcurge si inauntru + DirectoryMatches, ca foloseste un limbaj de search
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.java");
        System.out.println(matcher.matches(Paths.get("/home/java/gigi.java")));


        Path path1 = Paths.get("0*b/test/1");
        Path path2 = Paths.get("9\\*b/test/1");
        Path path3 = Paths.get("01b/test/1");
        Path path4 = Paths.get("0*b/1"); // The fourth path doesn't match  because there is no directory between "b" and "1" for the ** to match
        String glob = "glob:[0-9]\\*{A*,b}/**/1"; // The glob was looking for the literal asterisk by itself
        matches(path1, glob); // true
        matches(path2, glob); // false
        matches(path3, glob); // false
        matches(path4, glob); // false

        /*

        Remember that we are using a file system�specific PathMatcher. This means
slashes and backslashes can be treated differently, depending on what
operating system you happen to be running.

UNIX doesn't see the backslash as a directory boundary. The
lesson here is to use / instead of \\ so your code behaves more predictably
across operating systems.
         */
    }

    public static void matches(Path path, String glob) {
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(glob);
        System.out.println(matcher.matches(path));
    }
}
