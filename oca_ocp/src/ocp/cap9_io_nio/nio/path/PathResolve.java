package ocp.cap9_io_nio.nio.path;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathResolve {

    /*
    Resolve = cum ajung din sursa in destinatie
    Relativize = cum ma intorc din sursa in destinatie
     */

    /*
    Just like normalize(), keep in mind that resolve() will not check that the
directory or file actually exists.d
     */
    public static void main(String[] args) {
        Path dir = Paths.get("/home/java");
        Path file = Paths.get("models/Model.pdf");
        Path result = dir.resolve(file);
        System.out.println("result = " + result);

        /*
        path1.resolve(path2) should be read as "resolve path2 within path1's directory."
         */
        Path absolute = Paths.get("/home/java");
        Path abs2 = Paths.get("/home/users");
        Path relative = Paths.get("dir");
        Path file2 = Paths.get("Model.pdf");
        System.out.println("1: " + absolute.resolve(relative));
        System.out.println("2: " + absolute.resolve(file2));
        System.out.println("3: " + relative.resolve(file2));
        System.out.println("4: " + relative.resolve(absolute)); // BAD
        System.out.println("5: " + file2.resolve(absolute)); // BAD
        System.out.println("6: " + file2.resolve(relative)); // BAD
        System.out.println("7: " + abs2.resolve(absolute));
        // When the argument to resolve starts with the root (such as c: or, on *nix, a /), the result is same as the argument.
    }
}
