package ocp.cap9_io_nio.nio.path;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathIterator {

    public static void main(String[] args) {
        int spaces = 1;
        Path myPath = Paths.get("tmp", "dir1", "dir2", "dir3", "file.txt");
        for (Path subPath : myPath) {
            System.out.format("%" + spaces + "s%s%n", "", subPath); //
            spaces += 2;
        }
    }
}
