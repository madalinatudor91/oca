package ocp.cap9_io_nio.io;

public class ConsoleExample {

    public static void main(String[] args) {
        // You can read as well as write only character data from/to it.
        System.console().readPassword("format" /* var args */);
    }

}
