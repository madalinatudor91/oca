package ocp.cap9_io_nio.io;

import java.io.File;
import java.io.IOException;

public class DirectoryExample {

    static String[] dirs = {"dir1", "dir2"};

    public static void main(String[] args) throws IOException {
        for (String d : dirs) {
            String path = d;
            File file = new File(path, "file.txt");
            System.out.print(file.exists() + " ");
            file.createNewFile(); // creeaza doar fisier, nu si dir; daca exista fisierul, nu crapa, returneaza false
        }
    }

}
