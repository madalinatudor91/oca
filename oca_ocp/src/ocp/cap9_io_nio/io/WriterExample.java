package ocp.cap9_io_nio.io;

import java.io.*;

public class WriterExample {

    public static void main(String[] args) {
        char[] input = new char[50];
        int size = 0;

        /*
        Instances of the File class are immutable; that is, once created, the abstract pathname represented by a File object will never change.
         */

        File file = new File("writerExample.txt");
        try (FileWriter writer = new FileWriter(file)) {

            writer.write("howdy\nfolks\n");
            writer.flush();
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            FileReader reader = new FileReader(file);
            size = reader.read(input);

            System.out.println("size: " + size);
            for (char c: input) {
                System.out.print(c);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
