package ocp.cap11_collections.sets;

import java.util.TreeSet;

public class TreeTest {

    public static void main(String[] args) {
        TreeSet<Integer> set = new TreeSet<>();
        // NavigableSet e pt comparari: lower, etc.
        set.add(3);
        set.add(-4);
        set.add(-0);
        set.add(9);
        System.out.println(set);
    }
}
