package ocp.cap11_collections.iterators;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TestHasNext {

    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        strings.add("23424");
        strings.add("4trrgf");

        Iterator<String> it = strings.iterator();
        it.next();
        it.next();
        it.next(); // NoSuchElementException
    }
}
