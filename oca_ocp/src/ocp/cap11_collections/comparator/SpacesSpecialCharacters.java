package ocp.cap11_collections.comparator;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class SpacesSpecialCharacters {

    public static void main(String[] args) {
        String[] sa = {">ff<", "> f<", ">f <", ">FF<" };
        System.out.println(Arrays.toString(sa));
        Arrays.sort(sa);
        System.out.println(Arrays.toString(sa));
        // coduri ASCII
        // " " - 32
        // "A" - 65
        // "a" - 97

        Student[] students = {new Student(5), new Student(8), new Student(2), new Student(8)};
        Arrays.sort(students); // Student cannot be cast to java.lang.Comparable


        Comparator<Student> comp = Collections.reverseOrder();
        Comparator<Student> comp2 = Collections.reverseOrder(comp);

    }
}

class Student {

    int nimic;

    Student(int nimic) {
        this.nimic = nimic;
    }
}

/*
       methods that do not modify the collection (i.e. the threads that just "read" a collection) should acquire a
       read lock and threads that modify a collection should acquire a write lock.

A static nested class can contain a non - static inner class.

An enum is allowed to implement interfaces.
public enum EnumA implements I { A, AA, AAA};  //1

StandardOpenOption.CREATE, StandardOpenOption.READ do not make sense if put together and therefore an
IllegalArgumentException will be thrown in such cases. There are questions on the exam that expect you to know such combinations.
If the file already exists, TRUNCATE_EXISTING will take care of the existing content. If the file does not exist,
CREATE will ensure that it is created.
CREATE option will cause the file to be overwritten from the start of the file and it will NOT get rid of the existing content.
CREATE_NEW will cause an exception to be thrown if the file already exists.
WRITE will cause an exception to be thrown if the file does not exist. It will not get rid of the existing content either.
While TRUNCATE_EXISTING means that all existing content will be removed from the file, CREATE_NEW implies that the
 file should not exist already. Thus, if the file already exists, it will throw java.nio.file.FileAlreadyExistsException.


Identify the correct statements regarding the WatchService API provided by Java NIO.
        The count for ENTRY_MODIFY and  OVERFLOW is always 1.     FALSE For these event types, the count can be greater than 1.
        The counts for ENTRY_CREATE and ENTRY_DELETE are always 1.


The interesting thing to note here is that the String passed as the delimiter parameter is not used as a delimiter
itself but each character of that String used as a delimiter. Therefore, in this case, the delimiters are D, E, L, I, and M (not DELIM).
Therefore, the input String will be tokenized whereever D, E, L, I, and M are found.

This is a common mistake found in code that uses StringTokenizer.


This method works as if by invoking the two-argument split method with the given expression and a limit argument of zero.
Trailing empty strings are therefore not included in the resulting array.


JavaDoc API description for WatchKey says, "When initially created the key is said to be ready.
When an event is detected then the key is signaled and queued so that it can be retrieved by invoking the
watch service's poll or take methods. Once signalled, a key remains in this state until its reset method is invoked
to return the key to the ready state."

Note: It does not explicitly mention "invalid" as one of the states but in the description for cancel() method, it says,
"Cancels the registration with the watch service. Upon return the watch key will be invalid.
...
Once cancelled, a watch key remains forever invalid."




RandomAccessFile implements DataOutput, DataInput, Closeable {


Observe that the declaration of try-with-resources statement is wrong. The type of the resource must be specified in the try itself.
 i.e. it should be: try(Statement stmt = c.createStatement())



String str = "oo la la, shut the door, shut the door.";
    String rex = "\\Soo\\S|\\boo\\b";
    String replace = "OO";
    Pattern p = Pattern.compile(rex);
    Matcher m = p.matcher(str);
    String val = m.replaceAll(replace);
    System.out.println(val);

Note that \\boo\\b will match only 2 characters because \\b does not consume any character
while \\Soo\\S will match 4 characters because \\S does consume one character. Therefore, when \\Soo\\S is used to replace door,
 all the 4 characters will be replaced with OO and the result would be OO and not dOOr.

OO la la, shut the OO, shut the OO.





      FileWriter fw = new FileWriter("text.txt");
      // fw.write("hello"); //1
      fw.close();


It will create an empty file.
If the file already exists, it will be overwritten with a new file. To append to the existing file, the following constructor should be used.
public FileWriter(String fileName,  boolean append)


    The order of join() and compute() is critical. Remember that fork() causes the sub-task to be submitted to the pool and another thread
    can execute that task in parallel to the current thread. Therefore, if you call join() on the newly created sub task, you are basically
    waiting until that task finishes. This means you are using up both the threads (current thread and another thread from the pool that executes
    the subtask) for that sub task. Instead of waiting, you should use the current thread to compute another subtask and when done, wait for another thread to finish.
    This means, both the threads will execute their respective tasks in parallel instead of in sequence.

You can call filteredRowSet.setFilter(predicateInstance); to set the condition. predicateInstance is an instance of a class that implements the Predicate interface.



Remember that static fields are never serialized irrespective of whether they are marked transient or not.  In fact, making static fields as transient is redundant.



StandardWatchEventKinds : ENTRY_CREATE, nu ENTRY_CREATED
static WatchEvent.Kind<Path> ENTRY_CREATE
Directory entry created.

static WatchEvent.Kind<Path> ENTRY_DELETE
Directory entry deleted.

static WatchEvent.Kind<Path>ENTRY_MODIFY
Directory entry modified.

static WatchEvent.Kind<Object> OVERFLOW


        2. When you create a FileOutputStream without specifying the append mode (which can be true or false), it overwrites the existing file.



public interface Callable<V> {



Both can be used with an ExecutorService because ExecutorService has overloaded submit methods:
<T> Future<T> submit(Callable<T> task)
and
Future<?> submit(Runnable task) Observe that even though a Runnable's run() method cannot return a value, the
ExecutorService.submit(Runnable) returns a Future. The Future's get method will return null upon successful completion.




WatchKey take() throws InterruptedException
WatchKey poll(long timeout, TimeUnit unit) throws InterruptedException : Retrieves and removes the next watch key,
waiting if necessary up to the specified wait time if none are yet present.
WatchKey poll(): Retrieves and removes the next watch key, or null if none are present.



Lock.lock() returns void. Lock.tryLock() returns boolean.
  ReentrantLock rlock = new ReentrantLock();
        boolean f1 = rlock.lock();
        System.out.println(f1);
        boolean f2 = rlock.lock();
        System.out.println(f2);




During deserialization, the constructor of the class (or any static or instance blocks) is not executed. However,
if the super class does not implement Serializable, its constructor is called. So here, BooBoo and Boo are not Serializable so their constructor is invoked.

 */
