package ocp.cap11_collections.maps;

public class TestEqualHashCode {

    private int id;

    @Override
    public boolean equals(Object o) {
        return true;
    }

    @Override
    public int hashCode() {
        return (int) Math.random();
    }

    public static void main(String[] args) {
        int i = 3;
        System.out.println(++i * 5);
        System.out.println(i++ * 5);

        // si pana la urma in ce ordine se uita?
        TestEqualHashCode o1 = new TestEqualHashCode();
        if (o1.equals(new TestEqualHashCode())) {
            System.out.println("equals");
        } else {
            System.out.println("not equals");
        }
    }
}
