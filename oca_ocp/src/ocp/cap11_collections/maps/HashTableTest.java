package ocp.cap11_collections.maps;

import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

public class HashTableTest {

    public static void main(String[] args) {
        Hashtable<Integer, Object> map = new Hashtable<>();
//        map.put(null, null); // la ambele da null pointer exception
//        map.put(4, null); // null pointer
//        map.put(null, 4); // null pointer
        ConcurrentHashMap concMap = new ConcurrentHashMap();
        concMap.put(null, ""); // also null pointer

        System.out.println(map);
    }
}
