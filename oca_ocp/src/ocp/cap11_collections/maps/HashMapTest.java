package ocp.cap11_collections.maps;

import java.util.HashMap;
import java.util.Map;

public class HashMapTest {

    public static void main(String[] args) {
        Map<Integer, Object> map = new HashMap<>();
        map.put(1, "454545t");

        map.put(null, "34eerd");
        System.out.println(map.get(null));

        map.put(null, "gigic");
        System.out.println(map.get(null));

        map.put(5, null);
        System.out.println(map.get(5));
    }
}
