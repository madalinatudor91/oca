package ocp.cap11_collections.type_params;

import java.util.ArrayList;
import java.util.List;

public class ListSuperOrExtends {

    public static void main(String[] args) {
        List<Dog> dogs = new ArrayList<>();
//        stuff(dogs);
        correctStuff(dogs);

    }

    static void stuff(List<Animal> animals) {

    }

    static void correctStuff(List<? extends Animal> animals) {
//        animals.add(new Animal());
//        animals.add(new Dog());
        // nu compileaza daca se incearca adaugarea

    }
}

class Animal {
}

class Dog extends Animal {
}

class Cat extends Animal {
}