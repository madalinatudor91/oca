package ocp.cap11_collections.generics;

import java.util.ArrayList;
import java.util.List;

public class LegacyCode {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(3);
        addStrings(list);// nu crapa
        System.out.println(list);

        List list2 = new ArrayList<Integer>();
        list2.add("3r34e2e");

        List<Integer> list3 = new ArrayList();

    }

    public static void addStrings(List list) {
        list.add(new String("43"));
        list.add(new String("5"));
    }

}



