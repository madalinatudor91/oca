package ocp.cap11_collections.to_array_to_list;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ChangeAsList {

    public static void main(String[] args) {
        String[] array = {"a1", "a2", "a3"};
        List<String> list = Arrays.asList(array); // Arrays.asList tine ambele sincronizate

        System.out.println(list);

        array[0] = "new a1";
        System.out.println(list);

        list.set(0, "newer l1");
        System.out.println(Arrays.toString(array));

        System.out.println();

        // only one way
        List<String> strings = new ArrayList<>();
        strings.add("23");
        strings.add("3435");

        Object[] stringsArray = strings.toArray(); // WTF, intoarce object[]
        System.out.println(Arrays.toString(stringsArray));

        stringsArray[0] = "wtf";
        System.out.println(strings);
    }
}
