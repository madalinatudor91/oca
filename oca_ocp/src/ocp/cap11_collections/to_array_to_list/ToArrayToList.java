package ocp.cap11_collections.to_array_to_list;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ToArrayToList {

    public static void main(String[] args) {
        fromListToArray();
        System.out.println();
        fromArrayToList();
    }

    private static void fromArrayToList() {
        String[] array = {"one", "two", "three", "four"};
        List<String> list = Arrays.asList(array);

        System.out.println(Arrays.toString(array));
        System.out.println(list);
        System.out.println(list.hashCode());

        Arrays.sort(array);

        System.out.println(Arrays.toString(array));
        System.out.println(list);
        System.out.println(list.hashCode());
    }

    private static void fromListToArray() {
        List<String> list = new ArrayList<>();
        list.add("one");
        list.add("two");
        list.add("three");
        list.add("four");
        System.out.println(list);

        Object[] array = list.toArray();
        System.out.println(Arrays.toString(array));

        Collections.sort(list); // nu returneaza lista, deci o altereaza pe cea existenta

        System.out.println(list);
        System.out.println(Arrays.toString(array));


        // array-ul ramane neschimbat
    }

}
