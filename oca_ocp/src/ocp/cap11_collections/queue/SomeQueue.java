package ocp.cap11_collections.queue;

import java.util.Comparator;
import java.util.PriorityQueue;

public class SomeQueue {

    public static void main(String[] args) {
        int[] array = {1, 5, 3, 7, 6, 9, 8};

        PriorityQueue<Integer> pq1 = new PriorityQueue<>();
        for (int x : array) {
            pq1.offer(x); // insert
        }

        System.out.println();
        for (Integer x : pq1) {
            System.out.println(pq1.peek()); // intotdeauna primul element
        }

        System.out.println();
//        for (Integer el : pq1) { // ha ha ConcurrentModificationException pt ca iterez pe cv care nu si-a terminat treaba...
//            System.out.println(pq1.poll());
//        }
        for (int i = 0; i < pq1.size(); i++) { // incomplet in continuare, tb array.length, PT CA POLL SCOATE ELEMENTE
            System.out.println(pq1.poll());
        }

        System.out.println();
        for (int i = 0; i < array.length; i++) {
//            System.out.println(pq1.get()); // NU EXISTA GET!
        }

    }

    static class PQsort implements Comparator<Integer> {

        @Override
        public int compare(Integer o1, Integer o2) {
            return o1.compareTo(o2);
        }
    }

}
