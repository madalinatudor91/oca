package ocp.cap11_collections.queue;

import java.util.PriorityQueue;

public class PriorityQueueExample {

    // nici cu debugger nu inteleg...
    public static void main(String[] args) {
        PriorityQueue<String> pq = new PriorityQueue<>();
        pq.add("2");
        System.out.println(pq);
        pq.add("4");
        System.out.println(pq);
        System.out.print(pq.peek() + " ");
        System.out.println(pq);
        pq.offer("1");
        System.out.println(pq);
        pq.add("3");
        System.out.println(pq);
        pq.remove("1"); // In a priority queue, elements can be inserted in any order but removal of the elements is in a sorted order.
        System.out.println(pq);
        System.out.print(pq.poll() + " ");
        if (pq.remove("2")) System.out.print(pq.poll() + " ");
        System.out.println(pq.poll() + " " + pq.peek());
    }
}
