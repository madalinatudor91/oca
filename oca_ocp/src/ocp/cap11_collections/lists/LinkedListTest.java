package ocp.cap11_collections.lists;

import java.util.LinkedList;

public class LinkedListTest {

    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        list.addFirst(4); // cele cu last / fisrt sunt din deque, care extinde queue
        list.offer(8);
        list.addFirst(5);

        list.push(-4);// == addFirst...

        // mai sunt metode
        System.out.println(list);
    }
}
