package ocp.cap13_threads.wait_notify;

public class ThreadInteraction {

    public static void main(String[] args) {
        ThreadB thread = new ThreadB();
        thread.start();
        synchronized (thread) { // exemplu clasic de notify / wait / sync
            try {
                System.out.println("Waiting for b to complete...");
                thread.wait();
                System.out.println(thread.total);
            } catch (InterruptedException e) {// ATENTIE
                System.out.println("Total is " + thread.total);
            }
        }
    }
}

class ThreadB extends Thread {

    int total;

    @Override
    public void run() {
        synchronized (this) {
            for (int i = 0; i < 100; i++) {
                total += i;
            }
            notify();
        }
    }

}