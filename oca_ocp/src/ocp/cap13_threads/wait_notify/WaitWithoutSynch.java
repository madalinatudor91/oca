package ocp.cap13_threads.wait_notify;

public class WaitWithoutSynch {

    /*
    The code does not acquire a lock on t before calling t.wait(), so it throws
    an IllegalMonitorStateException. The method is synchronized, but it's not synchronized
    on t so the exception will be thrown. If the wait were placed inside a synchronized(t) block,
    then D would be correct: It prints XY with a 10-second delay between X and Y. */
    public static synchronized void main(String[] args) throws InterruptedException {
        Thread t = new Thread();
        synchronized (t) {
            t.start();
            System.out.print("X");
            t.wait(1000000000);
            System.out.print("Y");
        }
    }
}
