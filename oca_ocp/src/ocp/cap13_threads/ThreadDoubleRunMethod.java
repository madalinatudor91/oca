package ocp.cap13_threads;

public class ThreadDoubleRunMethod extends Thread {

//    public static void main(String[] args) {
////        Thread t = new ThreadDoubleRunMethod(new MyRunnable());
//    }


    public static synchronized void main(String[] args) throws InterruptedException {
        Thread t = new Thread();
        synchronized (t) {
            t.start();
            System.out.print("X");
            t.wait(10000);
            System.out.print("Y");
        }
    }

    @Override
    public void run() {
        System.out.println("My thread");
        try {
            this.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

class MyRunnable implements Runnable {

    @Override
    public void run() {
        System.out.println("My runnable");
    }
}
