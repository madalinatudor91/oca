package ocp.cap13_threads;

public class ThreeThreadsSleeping {

    public static void main(String[] args) {
        NameRunnable nr = new NameRunnable();
        Thread t1 = new Thread(nr, "Th1");
        Thread t2 = new Thread(nr, "Th2");
        Thread t3 = new Thread(nr, "Th3");
        t1.start();
        t2.start();
        t3.start(); // ordine "random" de revenire din sleep

//        t2.setPriority(-9); //java.lang.IllegalArgumentException
    }
}

class NameRunnable implements Runnable {

    @Override
    public void run() {
        for (int i = 1; i < 4 ; i++) {
            System.out.println("Run by " + Thread.currentThread().getName());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Shouldn't be here");
            }
        }
    }
}
