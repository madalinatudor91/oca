package ocp.cap13_threads;

public class ThreadStates {

    public static void main(String[] args) {
        Thread t = new Thread(); // New thread, not alive
        System.out.println(t.getState());
        t.start(); // runnable and who knows, running (orice ar fi, e alive
        System.out.println(t.getState());
        try {
            t.sleep(1000L); // -100L => java.lang.IllegalArgumentException: timeout value is negative
            System.out.println(t.getState());
            System.out.println("Do stuff...");
        } catch (InterruptedException e) {
            System.out.println("PAM PAM");
        }

        t.yield(); // metoda statica, zice tutror threadurilor cu prioritate mai mare sa ia avant, daca vrea si cpu..
        System.out.println(t.getState());
    }
}
