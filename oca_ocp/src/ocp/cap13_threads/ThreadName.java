package ocp.cap13_threads;

public class ThreadName {

    public static void main(String[] args) {
        Thread t1 = new Thread(new RunMe()/*, "gigel"*/); // Thread-0
        t1.start();

        Thread t2 = new Thread(); // Thread-1
        t2.start();
        System.out.println(t2.getName());

        System.out.println("main thread is " + Thread.currentThread().getName()); // main thread is named main

//        t2.start(); // IllegalThreadStateException
        t1.run();
    }
}

class RunMe implements Runnable {

    @Override
    public void run() {
        System.out.println("Thread name " + Thread.currentThread().getName());
        System.out.println("Thread id " + Thread.currentThread().getId());
    }
}