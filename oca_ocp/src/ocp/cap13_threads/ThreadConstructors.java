package ocp.cap13_threads;

public class ThreadConstructors {

    public static void main(String[] args) {
        Thread t = new Thread();
        t.start();
        // metoda din Thread de run are:
//        if (target != null) {
//            target.run();
//        }
        // deci nu face nimic
        new Thread("gigica");

        Thread t3 = new Thread(new Thread());
        t3.start(); // deschide thread


        new Thread().run(); // nu deschide niciun thread, doar ruleaza codul respectiv


        // Runnable are doar
//        public abstract void run();
    }

}
