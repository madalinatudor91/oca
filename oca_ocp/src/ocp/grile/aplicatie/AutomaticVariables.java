package ocp.grile.aplicatie;

public class AutomaticVariables {
    static int si = 10;
    int ii = 20;

    /*
    In computer programming, an automatic variable is a lexically-scoped variable which is allocated
    and de-allocated automatically when program flow enters and leaves the variable's scope.
    The term local variable is usually synonymous with automatic variable, since these are the same thing in many programming languages.
     */
    public static void inner() {
        int ai = 30; //automatic variable
        final int fai = 40; //automatic final variable
        class Inner {
            public Inner() {
                System.out.println(si + "    " + fai);
            }
        }
        new Inner();
    }

    public static void main(String[] args) {
        AutomaticVariables.inner();
    }
}
