package ocp.grile.aplicatie;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.Date;

public class FileAttributes {

/*
DosFileAttributeView
? DosFileAttributes DOS stands for Disk Operating System. It is part of
all Windows operating systems. Even Windows 8 has a DOS prompt available.

This view supports the following attributes in addition to BasicFileAttributeView:
readonly  Boolean
hidden  Boolean
system  Boolean
archive  Boolean

BasicFileAttributeView

This view supports only the following attributes:

"lastModifiedTime"  FileTime
"lastAccessTime"  FileTime
"creationTime"  FileTime
"size"  Long
"isRegularFile"  Boolean
"isDirectory"  Boolean
"isSymbolicLink"  Boolean
"isOther"  Boolean
"fileKey"  Object


PosixFileAttributeView
POSIX stands for Portable Operating System
Interface. This interface is implemented by both UNIX- and Linux-based
operating systems. You can remember this because POSIX ends in "x," as do
UNIX and Linux.

Unix based file systems provide this view. This view supports the following attributes in addition to BasicFileAttributeView:

"permissions" : Set<PosixFilePermission>
"group" : GroupPrincipal


FileOwnerAttributeView Used to set the primary owner of a file or directory.

? AclFileAttributeView Sets more advanced permissions on a file or directory.

 */

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("test");
        BasicFileAttributes basic = Files.readAttributes(path, BasicFileAttributes.class); // arunca IOExc
        System.out.println("create: " + basic.creationTime());
        System.out.println("access: " + basic.lastAccessTime());
        System.out.println("modify: " + basic.lastModifiedTime());
        System.out.println("directory: " + basic.isDirectory());

        FileTime lastUpdated = FileTime.fromMillis(new Date().getTime());
        FileTime now = FileTime.fromMillis(new Date().getTime());
        FileTime created = FileTime.fromMillis(new Date().getTime());
        BasicFileAttributeView basicView = Files.getFileAttributeView(path, BasicFileAttributeView.class); // "view" this time
        basicView.setTimes(lastUpdated, now, created); // set all three
    }

}
