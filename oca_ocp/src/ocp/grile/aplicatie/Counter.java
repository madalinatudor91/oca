package ocp.grile.aplicatie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Counter {

    public void increment() {
//        AtomicInteger i = 0;
        AtomicInteger i = new AtomicInteger(0);
        /*
        AtomicInteger allows you to manipulate an integer value atomically.
        It provides several methods for this purpose.
         */
        i.incrementAndGet();

//        List<?> list = new ArrayList<?>();
        List<?> list = new ArrayList<String>();
//        list.add(""); // ce accepta atunci?
    }
}