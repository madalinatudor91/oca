package ocp.grile.aplicatie;

public class ReadObjectWithSerializable {

/*
Since the serialVersionUID of the serialized class and the new class are same,
the file will be deserialized without any issue. The new fields will be initialized to their Java defaults
( because constructors and initializers are not invoked during deserialization).
So the values for id and and age will remain null and 0 respectively.
 */
}
