package ocp.grile.aplicatie;

public class DaemonThread {

    /*
    Daemon threads are meant to perform supporting tasks that make sense only if normal threads are running.
For example, the Garbage Collector thread is useful only if some user thread is running.
If all the user threads end then what is the use of GC thread? So the program should end when all the non-daemon threads end.
You can make any Thread (if you have permission) a daemon thread by calling setDaemon(true) before starting that thread.
You can stop any thread.
     */
}
