package ocp.grile.aplicatie;

public class FilePermissions {

/*
RandomAccessFile provides the following two constructors
-  RandomAccessFile(File file, String mode) throws FileNotFoundException
Creates a random access file stream to read from, and optionally to write to, the file specified by the File argument.

- RandomAccessFile(String name, String mode) throws FileNotFoundException
Creates a random access file stream to read from, and optionally to write to, a file with the specified name.

The mode argument specifies the access mode in which the file is to be opened. The permitted values and their meanings are:
"r": Open for reading only. Invoking any of the write methods of the resulting object will cause an IOException to be thrown.
"rw": Open for reading and writing. If the file does not already exist then an attempt will be made to create it.

"rws": Open for reading and writing, as with "rw", and also require that every update to the file's content or metadata be written synchronously to the underlying storage device.
"rwd": Open for reading and writing, as with "rw", and also require that every update to the file's content be written synchronously to the underlying storage device.

The "rws" and "rwd" modes work much like the force(boolean) method of the FileChannel class, passing arguments of true and false, respectively,
except that they always apply to every I/O operation and are therefore often more efficient.
If the file resides on a local storage device then when an invocation of a method of this class returns it is guaranteed
that all changes made to the file by that invocation will have been written to that device.
This is useful for ensuring that critical information is not lost in the event of a system crash.
If the file does not reside on a local device then no such guarantee is made.

The "rwd" mode can be used to reduce the number of I/O operations performed.
Using "rwd" only requires updates to the file's content to be written to storage;
using "rws" requires updates to both the file's content and its metadata to be written, which generally requires at least one more low-level I/O operation.
 */
}


