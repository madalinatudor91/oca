package ocp.grile.aplicatie;

import java.util.Arrays;
import java.util.Comparator;

public class BinarySearch {

    static String[] sa = { "d", "bbb", "aaaa" };

    // Since there is no string of length 2 in sa, nothing in sa matches the string "cc".
    // So the return value has to be negative.
    // Further, if the value "cc" were to be inserted in sa, it would have to be inserted after "d" i.e. at index 1.
    // Thus, the return value will be -(index+1) = -2.
    public static void main(String[] args) {
        Arrays.binarySearch(sa, "cc", new MyStringComparator());
        Arrays.binarySearch(sa, "c", new MyStringComparator());
    }
}

class MyStringComparator implements Comparator {

    public int compare(Object o1, Object o2) {
        int s1 = ((String) o1).length();
        int s2 = ((String) o2).length();
        return s1 - s2;
    }
}