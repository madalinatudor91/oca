package ocp.grile.aplicatie;

public class Printf {

    /*
    The ordinary index starts with the first format specifier that does not use explicit index.
    In this case,  it starts with the middle %s. Therefore, it prints the first argument which is A.
    < is used for relative indexing. It means you want to use the argument that was used for the previous format specifier.
    In this case, it was A. Therefore, it prints A again.
     */
    public static void main(String[] args) {
        System.out.printf("%1$s %s %<s", "A", "B", "C");
    }
}
