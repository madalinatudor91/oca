package ocp.grile.carte_google.pretest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class OldList {

    private static Integer test = 2;

    public static void main(String[] args) {
        List list1 = new ArrayList<>(Arrays.asList(1, "two", 3.0));
        List list2 = new LinkedList<>
                (Arrays.asList(new Integer(1), new Float(2.0F), new Double(3.0)));
        System.out.println(list1);
        for (Object o : list1) {
            System.out.println(o.getClass());
        }
        list1 = list2;
        for (Object element : list1) {
            System.out.println(element + " " + element.getClass());
        }

        OldList nullOb = null;
        System.out.println("value: " + nullOb.test);

        // Externalizable interface declares two methods, writeExternal() and
//        readExternal().
    }
}
