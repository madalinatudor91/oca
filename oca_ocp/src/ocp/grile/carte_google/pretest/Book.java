package ocp.grile.carte_google.pretest;

abstract class AbstractBook {
    public String name;
}
interface Sleepy {
    public String name = "undefined";
}
class Book extends AbstractBook implements Sleepy {
    public Book(String name) {
        super.name = name;
    }
    public static void main(String []args) {
        AbstractBook philosophyBook = new Book("Principia Mathematica");
        System.out.println("The name of the book is " + philosophyBook.name); // LINE B
    }
}