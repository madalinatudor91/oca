package ocp.grile.carte_google.per_capitol;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ResultSetSintax {

    public static void main(String[] args) {
        try (Statement statement = null;
             ResultSet resultSet = statement.executeQuery("SELECT * FROM contact")) { // deci exista sintaxa asta
             System.out.println(resultSet.getInt("id") + "\t"
                    + resultSet.getString("firstName") + "\t"
                    + resultSet.getString("lastName") + "\t"
                    + resultSet.getString("email") + "\t"
                    + resultSet.getString("phoneNo"));
        } catch (SQLException sqle) {
            System.out.println("SQLException");
        }
    }
}
