package ocp.grile.carte_google.per_capitol;

public class StringFormat {

    /*
    If you want to form a string and use it later rather than just printing it using the printf()
    method, you can use a static method in the String class�format(). We have reimplemented
    the printRow() method used in the last example using the format() method, as shown:
     */

    void printRow(String player, int matches, int goals){
        String str = String.format("%-15s \t %5d \t\t %d \t\t %.1f \n",
                player, matches, goals, ((float)goals/(float)matches));
        System.out.print(str);
    }
}
