package ocp.grile.carte_google.per_capitol;

public class LocaleStuff {
/*
Each locale can have three entries: the language, country, and variant. You can use standard
codes available for language and country to form locale tags. There are no standard tags for
variants; you can provide variant strings based on your need.
� The getter methods in the Locale class�such as getLanguage(), getCountry(), and
getVariant()�return codes; whereas the similar methods of getDisplayCountry(),
getDisplayLanguage(), and getDisplayVariant() return names



There are many ways to create or get a Locale object corresponding to a locale:
� Use the constructor of the Locale class.
� Use the forLanguageTag(String languageTag) method in the Locale class.
� Build a Locale object by instantiating Locale.Builder and then call setLanguageTag()
from that object.
� Use the predefined static final constants for locales in the Locale class.

 */
}

