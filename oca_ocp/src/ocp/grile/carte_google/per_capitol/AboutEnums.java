package ocp.grile.carte_google.per_capitol;

public class AboutEnums {

    /*
    Enums are implicitly declared public, static, and final, which means you cannot extend them.
� When you define an enumeration, it implicitly inherits from java.lang.Enum. Internally,
enumerations are converted to classes. Further, enumeration constants are instances of the
enumeration class for which the enumeration constants are declared as members.
� If you declare an enum within a class, then it is by default static.
     */
}
