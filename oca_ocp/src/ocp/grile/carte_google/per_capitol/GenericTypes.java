package ocp.grile.carte_google.per_capitol;

class LastError<T> {
    private T lastError;

    void setError(T t) {
        lastError = t;
        System.out.println("LastError: setError");
    }
}

class StrLastError<S extends CharSequence> extends LastError<String> {
    public StrLastError(S s) {
    }

    void setError(S s) {
        System.out.println("StrLastError: setError");
    }
}

/*
At the time of compilation, the knowledge of type S is not available. Therefore,
the compiler records the signatures of these two methods as setError(String) in superclass and
setError(S_extends_CharSequence) in subclass�treating them as overloaded methods (not overridden).
In this case, when the call to setError() is found, the compiler finds both the overloaded methods matching,
resulting in the ambiguous method call error.
 */
public class GenericTypes {
    public static void main(String[] args) {
        StrLastError<String> err = new StrLastError<String>("Error");
//        err.setError("Last error"); // ambiguous call
        // la compilare pare ok, la runtime nu mai compileaza...
    }

    /*
    Use the extends keyword for both class type and interface when specifying bounded types
in generics. For specifying multiple base types, use the & symbol. For example, in List<?
extends X & Y>, ? will match types, extending both the types X and Y.

     In general, when you use wildcard parameters, you cannot call methods that modify the
object. If you try to modify, the compiler will give error messages. However, you can call
methods that access the object.
     */
}