package ocp.grile.carte_google.per_capitol;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class USPresident {
    private int term;

    public USPresident(String s, String s1, int i) {
    }

    public void setTerm(int term) {
        this.term = term;
    }

    /*
    Yes, it will print 56 even though you changed the term using its setter to 57 and
serialized again. This happens due to serialVersionUID, which is checked by the JVM at
the time of serialization. If a class is already serialized and you try to serialize it again, the
JVM will not serialize it.
     */
    public static void main(String[] args) {
        USPresident usPresident = new USPresident("Barack Obama", "2009 to --", 56);
        try (ObjectOutputStream oos = new ObjectOutputStream(new
                FileOutputStream("USPresident.data"))) {
            oos.writeObject(usPresident);
            usPresident.setTerm(57);
            oos.writeObject(usPresident);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
