package ocp.grile.carte_google.per_capitol;

public class StaticInitializerExceptions {

    /*
    Static initialization blocks cannot throw any checked exceptions. Non-static initialization
blocks can throw checked exceptions; however, all the constructors should declare that
exception in their throws clause
     */
}
