package ocp.grile.carte_google.per_capitol;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ResultSetInsert {

    public static void main(String[] args) throws SQLException {
        try (Connection connection = null;
             Statement statement =
                     connection.createStatement(
                             ResultSet.TYPE_SCROLL_SENSITIVE,
                             ResultSet.CONCUR_UPDATABLE);
             ResultSet resultSet =
                     statement.executeQuery("SELECT * FROM familyGroup")) {
            resultSet.moveToInsertRow();
            resultSet.updateString("COLUMN", "VALUE");
            resultSet.insertRow(); // INSERT ROW
            System.out.println("Table updated with a row. . .");
        }
    }
}
