package ocp.grile.carte_google.per_capitol;

public class Constructors {
    /*
    A final variable must be initialized. If it�s not initialized when it is declared, it must be
initialized in all the constructors. Also, a final variable can be assigned only once.
     */
}
