package ocp.grile.carte_google.per_capitol;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class ConnectionReadFile {

    public static void main(String[] args) throws Exception{
        FileInputStream findings = new FileInputStream("log.txt");
        DataInputStream dataStream = new DataInputStream(findings);
        BufferedReader br = new BufferedReader(new InputStreamReader(dataStream));
        String line;
        while ((line = br.readLine()) != null) {
            System.out.println(line);
        }
        /*
        the br.close() statement will close the BufferedReader object and the underlying
            stream objects referred by findings and dataStream.
         */
        br.close();
    }
}
