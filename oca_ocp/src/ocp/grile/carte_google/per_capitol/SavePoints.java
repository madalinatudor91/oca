package ocp.grile.carte_google.per_capitol;

import java.sql.Connection;
import java.sql.SQLException;

public class SavePoints {

    public static void main(String[] args) throws SQLException {
        Connection connection = null;
        // assume that all operations execute in this program successfully without
        // throwing any exceptions; also assume that the connection is established
        // successfully
        connection.setAutoCommit(false);
        // insert a row into the table here
        // create a savepoint in this transaction here
        //insert another row into the table here
        //create a named savepoint in this transaction here
        //insert the third row into the table here
        connection.rollback();
        connection.commit();

        /*
        Since connection.rollback(); is called before connection.commit(), all operations will be undone and no
    rows will be inserted into the table.)
         */
    }
}
