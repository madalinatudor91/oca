package ocp.grile.carte_google.per_capitol;

public class ResourceBundleStuff {
/*
    The class ResourceBundle has two derived classes: PropertyResourceBundle and
    ListResourceBundle. You can use ResourceBundle.getBundle() to automatically load a
    bundle for a given locale.

    NumberFormat has many factory methods: getInstance(), getCurrencyInstance(),
    getIntegerInstance(), and getPercentInstance().
*/
}
