package ocp.grile.carte_google.per_capitol;

public class InitCauseException {


    public static void main(String[] args) {
        try {
            foo();
        } catch (Exception re) {
            System.out.println(re.getClass());
        }
    }

    private static void foo() {
        try {
            throw new ArrayIndexOutOfBoundsException();
        } catch (ArrayIndexOutOfBoundsException oob) {
            RuntimeException re = new RuntimeException(oob); // in spate face this.cause = cause;
            re.initCause(re); // Can't overwrite cause with ...
            throw re;
        }
    }
}
