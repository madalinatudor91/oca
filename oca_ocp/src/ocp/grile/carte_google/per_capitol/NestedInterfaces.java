package ocp.grile.carte_google.per_capitol;

public class NestedInterfaces {

    /*
    An interface can be declared within another interface or class. Such interfaces are known as
nested interfaces.
� Unlike top-level interfaces that can have only public or default access, a nested interface can
be declared as public, protected, or private
     */
}
