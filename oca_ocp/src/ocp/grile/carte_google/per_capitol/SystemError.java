package ocp.grile.carte_google.per_capitol;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class SystemError {

    public static void main(String[] args) throws FileNotFoundException {
        OutputStream os = new FileOutputStream("log.txt");
        // this code segment redirects the System.err to the log.txt file and will
        // write the text �error� to that file.
        System.setErr(new PrintStream(os)); // SET SYSTEM.ERR
        System.err.println("Error");
    }
}
