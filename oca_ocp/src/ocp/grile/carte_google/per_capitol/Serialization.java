package ocp.grile.carte_google.per_capitol;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Serialization {
    /*
One more thing requires attention here�serialVersionUID. In this example, it�s set it to 1. If you are
implementing Serializable and not defining serialVersionUID, you will get a warning message. In fact, if you don�t
define it, JVM will define it for you; JVM will compute it based on the class behavior. But why it is required? Well, it is
there to prevent mistakenly loading a wrong version of a class while deserializing. Also, defining serialVersionUID
enables the serialized program to work across different JVM implementations seamlessly (which might not be a case
when you are not defining it explicitly). The bottom line: whenever you make a change in a serialized class, do not
forget to change the serialVersionUID also.
     */

    public static void main(String[] args) {
        USPresident usPresident = new USPresident("Barack Obama", "2009 to --", 56);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("USPresident.data"))){
            oos.writeObject(usPresident);
            usPresident.setTerm(57);
            oos.writeObject(usPresident);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
    /*
    If you deserialize the object and print the field term (term is declared as int and is not a
            transient), what it will print?
    A. 56
    B. 57
    C. null
    D. Compiler error
    E. R untime exception
    Answer: A. 56
            (Yes, it will print 56 even though you changed the term using its setter to 57 and
    serialized again. This happens due to serialVersionUID, which is checked by the JVM at
    the time of serialization. If a class is already serialized and you try to serialize it again, the
    JVM will not serialize it.)
    */
}
