package ocp.descoperite;

public class SwitchInitialization {

    public static void main(String[] args) {
        String stringForSwitch = "test";
        switch (stringForSwitch) {
            case "test1":
                Integer i = null;
                break;
            case "test2":
                // Integer i = null;
                // de revazut de ce nu merge o noua variabila i, desi switch-ul e ca un if...
                // poate "nu simte" break-ul?
                break;

        }
    }

}
