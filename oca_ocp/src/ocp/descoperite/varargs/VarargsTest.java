package ocp.descoperite.varargs;

import java.util.Arrays;

public class VarargsTest {

    public static void main(String[] args) {
        test();
        test("erfdfsdf");
        test("fdxc", "fdc");
    }

    public static void test(String... strings) {
        System.out.println(Arrays.toString(strings));
    }

}
