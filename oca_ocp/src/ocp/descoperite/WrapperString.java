package ocp.descoperite;

public class WrapperString {

    public static void main(String[] args) {
        String s1 = "a";
        String s2 = new String("a");
        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println(s1 == s2);
        System.out.println(s1.equals(s2));

        Integer i1 = 1;
        Integer i2 = 1; //new Integer(1); => da false la ==
        System.out.println(i1.hashCode());
        System.out.println(i2.hashCode());
        System.out.println(i1 == i2);
        System.out.println(i1.equals(i2));
    }

}
