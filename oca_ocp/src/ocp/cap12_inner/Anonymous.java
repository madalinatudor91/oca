package ocp.cap12_inner;

public class Anonymous {

    Popcorn p = new Popcorn() {

        @Override
        void pampam() {
            System.out.println("second");
        }

        void inaccesibila() {
            System.out.println("Aceasta metoda nu poate fi apelata in exterior");
        }

    }; // neaparat ;

    void test() {
        p.pampam();
//        p.inaccesibila();
    }


    public static void main(String[] args) {
        Thread al = new Thread("") { // se poate constructor cu params

        };
    }

}

class Popcorn {

    void pampam() {
        System.out.println("first");
    }

}