package ocp.cap12_inner;

import old_oca.grile.flori.p3_q4.Outer;

public class MyOuter {
    private int x = 7;
    private int abc = new MyInner().abc;

    public void makeInner() {
        MyInner in = new MyInner();
        in.seeOuter();
    }

    // Static nested classes may have static members, whereas the other flavors of nested classes may not.
    class MyInner extends MyOuter { // extends can be used!
//        private static int t = 0;

//        private int x = 9;

        private static final String gigel = "";

        private int abc = 90;

        public void seeOuter() {
            System.out.println("Outer x is " + x);
            System.out.println("Inner class ref is " + this);
            System.out.println("Outer class ref is " + MyOuter.this);
            System.out.println("Outer x again " + MyOuter.this.x);
        }
    }

    static class MyStaticInner extends MyOuter {
        int z = 9;

        void test() {

        }

    }

    public static void main(String[] args) {
        MyOuter.MyInner inner = new MyOuter().new MyInner();
        inner.seeOuter();
    }

    abstract class AnotherInner {
        String s;

        void test() {
            System.out.println(this.s);
            System.out.println(abc == MyOuter.this.abc);
        }

    }
}