package ocp.cap12_inner;

public class MethodLocalInner {

    void doStuff() {
        final String firstMethodString = "string, little string";
//        Gigi g = new Gigi(); // e normal, se executa metoda in ordine
        abstract class Gigi { // merge si cu final

            void gigiMethod() {
                System.out.println(firstMethodString); // doar cu final e acceptat
            }

        }

        class Gigica extends Gigi {

            void gigica() {
                System.out.print("gigica");
            }
        }

        Gigi g = new Gigi() {
            @Override
            void gigiMethod() {
                super.gigiMethod();
            }
        };

    }
}
